package com.projectA5.pacilclass.buat_quiz.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "jawaban")
@Data
@NoArgsConstructor
public class Jawaban {

    @Id
    @Column(name = "jawaban_id")
    @GeneratedValue
    private int jawabanId;

    @Column(columnDefinition = "TEXT")
    private String teksJawaban;

    // A, B, C, D, E
    @Column
    private String opsiJawaban;

    @ManyToOne
    @JoinColumn(name = "pertanyaan_id")
    @ToString.Exclude
    private Pertanyaan pertanyaan;

}
