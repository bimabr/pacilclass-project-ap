package com.projectA5.pacilclass.buat_quiz.model.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ResponseDto {

    private String url;

    public ResponseDto(String url) {
        this.url = url;
    }

}
