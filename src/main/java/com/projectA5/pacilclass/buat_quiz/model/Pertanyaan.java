package com.projectA5.pacilclass.buat_quiz.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "pertanyaan")
@Data
@NoArgsConstructor
public class Pertanyaan {

    @Id
    @GeneratedValue
    @Column(name = "pertanyaan_id")
    private int pertanyaanId;

    @Column(name = "question_number")
    private int questionNumber;

    @Column(columnDefinition = "TEXT", nullable = false)
    private String question;

    @Column(length = 1, name = "answer_key")
    private String answerKey;

    @ManyToOne
    @JoinColumn(name = "quiz_id")
    @ToString.Exclude
    private Quiz quiz;

    @OneToMany(mappedBy = "pertanyaan", cascade = {CascadeType.ALL})
    @ToString.Exclude
    private List<Jawaban> jawabans = new ArrayList<>();

}
