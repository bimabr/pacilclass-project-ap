package com.projectA5.pacilclass.buat_quiz.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class QuizDto {
    private String quizName;
    private int timeLimit;
    private String startDate;
    private String endDate;
    private List<PertanyaanDto> questions = new ArrayList<>();
}
