package com.projectA5.pacilclass.buat_quiz.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "quiz")
@Data
@NoArgsConstructor
public class Quiz {

    @Id
    @GeneratedValue
    @Column(name = "quiz_id")
    private Integer quizId;

    @Column(name = "quiz_name")
    private String quizName;

    @Column(name = "start_date")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private Date startDate;

    @Column(name = "end_date")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private Date endDate;

    @Column(name = "time_limit")
    private int timeLimit;

    @OneToMany(mappedBy = "quiz", cascade = {CascadeType.ALL})
    @ToString.Exclude
    private List<Pertanyaan> pertanyaans = new ArrayList<>();


}

