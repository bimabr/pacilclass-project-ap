package com.projectA5.pacilclass.buat_quiz.model.dto;


import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class PertanyaanDto {

    private int questionNumber;

    private String question;

    private String answerKey;

    private List<JawabanDto> answers = new ArrayList<>();

}
