package com.projectA5.pacilclass.buat_quiz.repository;

import com.projectA5.pacilclass.buat_quiz.model.Jawaban;
import com.projectA5.pacilclass.buat_quiz.model.Pertanyaan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JawabanRepository extends JpaRepository<Jawaban, Integer> {

    Jawaban findByJawabanId(int jawabanId);

    Iterable<Jawaban> findByPertanyaan(Pertanyaan pertanyaan);

}
