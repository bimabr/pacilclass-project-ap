package com.projectA5.pacilclass.buat_quiz.repository;

import com.projectA5.pacilclass.buat_quiz.model.Quiz;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface QuizRepository extends JpaRepository<Quiz, Integer> {

    Quiz findByQuizId(int quizId);

    Quiz findByQuizName(String quizName);

    @Transactional
    void deleteByQuizId(int quizId);
}
