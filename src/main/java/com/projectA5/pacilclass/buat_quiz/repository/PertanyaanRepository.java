package com.projectA5.pacilclass.buat_quiz.repository;

import com.projectA5.pacilclass.buat_quiz.model.Pertanyaan;
import com.projectA5.pacilclass.buat_quiz.model.Quiz;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PertanyaanRepository extends JpaRepository<Pertanyaan, Integer> {

    Pertanyaan findByPertanyaanId(int pertanyaanId);

    Pertanyaan findByQuestionNumberAndQuiz(int questionNumber, Quiz quiz);
}
