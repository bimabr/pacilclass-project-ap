package com.projectA5.pacilclass.buat_quiz.controller;

import com.projectA5.pacilclass.administrasi_kelas.model.Kelas;
import com.projectA5.pacilclass.administrasi_kelas.repository.KelasRepository;
import com.projectA5.pacilclass.buat_quiz.model.Quiz;
import com.projectA5.pacilclass.buat_quiz.model.dto.QuizDto;
import com.projectA5.pacilclass.buat_quiz.model.dto.ResponseDto;
import com.projectA5.pacilclass.buat_quiz.service.PertanyaanService;
import com.projectA5.pacilclass.buat_quiz.service.QuizService;
import com.projectA5.pacilclass.buat_quiz.service.error.InvalidDateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.ParseException;

@Controller
@RequestMapping("/buat-quiz")
public class QuizController {

    @Autowired
    private QuizService quizService;

    @Autowired
    private PertanyaanService pertanyaanService;


    @Autowired
    private KelasRepository kelasRepository;


    @GetMapping("")
    public String  createRestQuiz(@RequestParam(name = "kelasId") int id, Model model, HttpServletRequest request) {
        model.addAttribute("id", id);
        return "buat_quiz/add_quiz_new";
    }


    @PostMapping(value = "/validate", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity checkQuizForm(@RequestBody QuizDto quizDto) throws ParseException {
        try {
            return ResponseEntity.ok(quizService.validate(quizDto));
        } catch (InvalidDateException ex) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/save", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity saveQuiz(@RequestBody QuizDto quizDto, @RequestParam(name= "id") int id) throws ParseException {
        Quiz q = quizService.createQuizDto(quizDto);
        pertanyaanService.createPertanyaanFromDto(quizDto.getQuestions(), q);

        Kelas k = kelasRepository.findByid(id);
        k.getQuizzes().add(q);
        kelasRepository.save(k);

        return ResponseEntity.ok(new ResponseDto("/administrasi_kelas/kelas?id=" + id));
    }
}
