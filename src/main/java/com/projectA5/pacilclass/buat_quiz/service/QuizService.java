package com.projectA5.pacilclass.buat_quiz.service;

import com.projectA5.pacilclass.buat_quiz.model.Quiz;
import com.projectA5.pacilclass.buat_quiz.model.dto.QuizDto;
import com.projectA5.pacilclass.buat_quiz.service.error.InvalidDateException;

import java.text.ParseException;
import java.util.Date;

public interface QuizService {
    Quiz createQuiz(Quiz quiz);

    Quiz getQuizByName(String name);

    Quiz getQuizById(int id);

    void deleteQuiz(int id);

    Quiz updateQuiz(int id, Quiz quiz);

    Date getStartDate(int id);

    Date getEndDate(int id);

    int getTimeLimit(int id);

    QuizDto validate(QuizDto quizDto) throws ParseException, InvalidDateException;

    Quiz createQuizDto(QuizDto quizDto) throws ParseException;
}
