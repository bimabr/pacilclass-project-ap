package com.projectA5.pacilclass.buat_quiz.service;

import com.projectA5.pacilclass.buat_quiz.model.*;
import com.projectA5.pacilclass.buat_quiz.model.dto.QuizDto;
import com.projectA5.pacilclass.buat_quiz.repository.JawabanRepository;
import com.projectA5.pacilclass.buat_quiz.repository.PertanyaanRepository;
import com.projectA5.pacilclass.buat_quiz.repository.QuizRepository;
import com.projectA5.pacilclass.buat_quiz.service.error.InvalidDateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class QuizServiceImpl implements QuizService {

    @Autowired
    private QuizRepository quizRepository;

    @Autowired
    private JawabanRepository jawabanRepository;

    @Autowired
    private PertanyaanRepository pertanyaanRepository;

    @Override
    public Quiz createQuiz(Quiz quiz) {
        quizRepository.save(quiz);
        return quiz;
    }

    @Override
    public Quiz getQuizByName(String name){
        return quizRepository.findByQuizName(name);
    }

    @Override
    public Quiz getQuizById(int id) {
        return quizRepository.findByQuizId(id);
    }

    @Override
    public void deleteQuiz(int id) {
        quizRepository.deleteByQuizId(id);
    }

    @Override
    public Quiz updateQuiz(int id, Quiz quiz) {
        Quiz oldQuiz = quizRepository.findByQuizId(id);
        oldQuiz.setQuizName(quiz.getQuizName());
        oldQuiz.setStartDate(quiz.getStartDate());
        oldQuiz.setEndDate(quiz.getEndDate());
        oldQuiz.setTimeLimit(quiz.getTimeLimit());
        quizRepository.save(oldQuiz);

        return oldQuiz;
    }

    @Override
    public Date getStartDate(int id) {
        Quiz q = quizRepository.findByQuizId(id);
        return q.getStartDate();
    }

    @Override
    public Date getEndDate(int id) {
        Quiz q = quizRepository.findByQuizId(id);
        return q.getEndDate();
    }

    @Override
    public int getTimeLimit(int id) {
        Quiz q = quizRepository.findByQuizId(id);
        return q.getTimeLimit();
    }

    @Override
    public Quiz createQuizDto(QuizDto quizDto) throws ParseException {
        Quiz q = new Quiz();
        q.setQuizName(quizDto.getQuizName());
        q.setTimeLimit(quizDto.getTimeLimit());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        Date start = sdf.parse(quizDto.getStartDate());
        Date end = sdf.parse(quizDto.getEndDate());

        q.setStartDate(start);
        q.setEndDate(end);

        return quizRepository.save(q);
    }

    @Override
    public QuizDto validate(QuizDto quizDto) throws ParseException, InvalidDateException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        Date start = sdf.parse(quizDto.getStartDate());
        Date end = sdf.parse(quizDto.getEndDate());

        if (end.before(start))
            throw new InvalidDateException("Date input is invalid");

        return quizDto;
    }

}
