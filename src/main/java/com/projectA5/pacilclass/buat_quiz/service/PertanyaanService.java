package com.projectA5.pacilclass.buat_quiz.service;

import com.projectA5.pacilclass.buat_quiz.model.Jawaban;
import com.projectA5.pacilclass.buat_quiz.model.Pertanyaan;
import com.projectA5.pacilclass.buat_quiz.model.Quiz;
import com.projectA5.pacilclass.buat_quiz.model.dto.PertanyaanDto;

import java.util.List;

public interface PertanyaanService {

    Pertanyaan createPertanyaan(int quizId, Pertanyaan pertanyaan);

    Pertanyaan getPertanyaanByNumber(int questionNumber, int quizId);

    List<Jawaban> getListJawaban(int pertanyaanId);

    void deletePertanyaan(int pertanyaanId);

    void createPertanyaanFromDto(List<PertanyaanDto> questions, Quiz q);

}
