package com.projectA5.pacilclass.buat_quiz.service;

import com.projectA5.pacilclass.buat_quiz.model.Jawaban;
import com.projectA5.pacilclass.buat_quiz.model.Pertanyaan;
import com.projectA5.pacilclass.buat_quiz.repository.JawabanRepository;
import com.projectA5.pacilclass.buat_quiz.repository.PertanyaanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JawabanServiceImpl implements JawabanService {

    @Autowired
    JawabanRepository jawabanRepository;

    @Autowired
    PertanyaanRepository pertanyaanRepository;

    @Override
    public Jawaban createJawaban(int pId, Jawaban jawaban) {
        Pertanyaan p = pertanyaanRepository.findByPertanyaanId(pId);

        p.getJawabans().add(jawaban);
        jawaban.setPertanyaan(p);
        return jawabanRepository.save(jawaban);
    }

    @Override
    public Jawaban updateJawaban(Jawaban jawaban) {
        return jawabanRepository.save(jawaban);
    }

    @Override
    public Jawaban getJawaban(int id) {
        return jawabanRepository.findByJawabanId(id);
    }

    @Override
    public void deleteJawaban(int id) {
        jawabanRepository.deleteById(id);
    }

}
