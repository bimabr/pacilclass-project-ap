package com.projectA5.pacilclass.buat_quiz.service;

import com.projectA5.pacilclass.buat_quiz.model.Jawaban;

public interface JawabanService {

    Jawaban createJawaban(int pertanyaanId, Jawaban jawaban);

    Jawaban updateJawaban(Jawaban jawaban);

    Jawaban getJawaban(int id);

    void deleteJawaban(int id);
}
