package com.projectA5.pacilclass.buat_quiz.service;

import com.projectA5.pacilclass.buat_quiz.model.Jawaban;
import com.projectA5.pacilclass.buat_quiz.model.Pertanyaan;
import com.projectA5.pacilclass.buat_quiz.model.dto.JawabanDto;
import com.projectA5.pacilclass.buat_quiz.model.dto.PertanyaanDto;
import com.projectA5.pacilclass.buat_quiz.model.Quiz;
import com.projectA5.pacilclass.buat_quiz.repository.JawabanRepository;
import com.projectA5.pacilclass.buat_quiz.repository.PertanyaanRepository;
import com.projectA5.pacilclass.buat_quiz.repository.QuizRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PertanyaanServiceImpl implements PertanyaanService{

    @Autowired
    private PertanyaanRepository pertanyaanRepository;

    @Autowired
    private QuizRepository quizRepository;

    @Autowired
    private JawabanRepository jawabanRepository;

    @Override
    public Pertanyaan createPertanyaan(int quizId, Pertanyaan pertanyaan) {
        Quiz quiz = quizRepository.findByQuizId(quizId);

        pertanyaan.setQuiz(quiz);
        quiz.getPertanyaans().add(pertanyaan);

        pertanyaanRepository.save(pertanyaan);
        return pertanyaan;
    }

    @Override
    public Pertanyaan getPertanyaanByNumber(int questionNumber, int quizId) {
        Quiz quiz = quizRepository.findByQuizId(quizId);
        return pertanyaanRepository.findByQuestionNumberAndQuiz(questionNumber, quiz);
    }

    @Override
    public List<Jawaban> getListJawaban(int pId) {
        return pertanyaanRepository.findByPertanyaanId(pId).getJawabans();
    }

    @Override
    public void deletePertanyaan(int pertanyaanId) {
        pertanyaanRepository.deleteById(pertanyaanId);
    }

    @Override
    public void createPertanyaanFromDto(List<PertanyaanDto> questions, Quiz q) {
        questions.forEach(pertanyaanDto -> {
            Pertanyaan p = new Pertanyaan();
            p.setAnswerKey(pertanyaanDto.getAnswerKey());
            p.setQuestion(pertanyaanDto.getQuestion());
            p.setQuestionNumber(pertanyaanDto.getQuestionNumber());
            p.setQuiz(q);

            pertanyaanRepository.save(p);

            createJawaban(pertanyaanDto.getAnswers(), p);

            q.getPertanyaans().add(p);
        });
    }

    private void createJawaban(List<JawabanDto> answers, Pertanyaan p) {
        answers.forEach(jawabanDto -> {
            Jawaban j = new Jawaban();
            j.setTeksJawaban(jawabanDto.getTeksJawaban());
            j.setOpsiJawaban(jawabanDto.getOpsiJawaban());
            j.setPertanyaan(p);

            jawabanRepository.save(j);

            p.getJawabans().add(j);
        });
    }


}
