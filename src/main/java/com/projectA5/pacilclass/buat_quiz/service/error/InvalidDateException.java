package com.projectA5.pacilclass.buat_quiz.service.error;

public final class InvalidDateException extends RuntimeException {

    private static final long serialVersionUID = 2781912855688935021L;

    public InvalidDateException(final String message) {
        super(message);
    }
}
