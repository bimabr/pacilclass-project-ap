package com.projectA5.pacilclass.akses_quiz.repository;

import com.projectA5.pacilclass.akses_quiz.model.PesertaQuiz;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PesertaQuizRepository extends JpaRepository<PesertaQuiz, Integer> {
    PesertaQuiz findByPqid(int pqid);
    Iterable<PesertaQuiz> findByQuizIdAndIdUser(int quizId, int idUser);
}
