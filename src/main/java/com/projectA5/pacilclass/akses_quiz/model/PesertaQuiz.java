package com.projectA5.pacilclass.akses_quiz.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "PesertaQuiz")
@Data
@NoArgsConstructor
public class PesertaQuiz {

    @Id
    @GeneratedValue
    @Column(name = "pesertaQuiz_id")
    private int pqid;

    @Column(name = "nilai")
    private int nilai;

    @Column(name = "kcjb")
    private String kunci;

    @Column(name = "id_quiz")
    private int quizId;

    @Column(name = "Expired")
    private boolean expired;

    @Column(name ="idUser")
    private int idUser;

    public PesertaQuiz(int nilai,String kunci,int quizId,int idUser){
        this.nilai=nilai;
        this.kunci=kunci;
        this.quizId = quizId;
        this.expired = false;
        this.idUser = idUser;
    }

    public boolean getExpired(){
        return this.expired;
    }

}

