package com.projectA5.pacilclass.akses_quiz.controller;

import com.projectA5.pacilclass.akses_quiz.model.PesertaQuiz;
import com.projectA5.pacilclass.akses_quiz.repository.PesertaQuizRepository;
import com.projectA5.pacilclass.akses_quiz.service.AksesQuizService;
import com.projectA5.pacilclass.authentication.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Iterator;
import java.util.List;


@Controller
@RequestMapping(path="/akses_quiz")
public class AksesQuizController {

    @Autowired
    private AksesQuizService aksesQuizService;

    @Autowired
    private PesertaQuizRepository pesertaQuizRepository;

    @Autowired
    private UserRepository userRepository;

    @GetMapping(path = "")
    public String keluarinQuiz(
            @RequestParam(value = "idQuiz")int idQuiz,
            @RequestParam(value = "nomor")int nomor,
            @RequestParam(value = "batasWaktu")String batasWaktu,
            @RequestParam(value = "pqid")int pqid,
            Model model) throws Exception{
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        if(aksesQuizService.expired(batasWaktu,pqid) || aksesQuizService.lastQuestion(idQuiz,nomor)) {
            aksesQuizService.setTruePeserta(pqid,idQuiz);
            String name = authentication.getName();
            int id = userRepository.findByUsername(name).getUserId();
            Iterable<PesertaQuiz> pqTmp = pesertaQuizRepository.findByQuizIdAndIdUser(idQuiz,id);
            Iterator<PesertaQuiz> iter = pqTmp.iterator();
            model.addAttribute("nilai",iter.next().getNilai());
            return "akses_quiz/end";
        }
        String soals = aksesQuizService.getSoal(idQuiz, nomor);
        List<String> jawabans = aksesQuizService.getJawabanBySoal(nomor, idQuiz);
        model.addAttribute("Jawabans", jawabans);
        model.addAttribute("pqid", pqid);
        model.addAttribute("Soals", soals);
        model.addAttribute("nomor", nomor);
        model.addAttribute("idQuiz", idQuiz);
        model.addAttribute("batasWaktu", batasWaktu);
        return "akses_quiz/tampilan_quiz";
    }

    @GetMapping(path = "/start")
    public String startLandingPage (
            @RequestParam(value = "idQuiz")int idQuiz,
            Model model){
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        model.addAttribute("idQuiz",idQuiz);
        boolean expired = aksesQuizService.dateCheck(idQuiz);
        if(expired){
            String name = authentication.getName();
            int id = userRepository.findByUsername(name).getUserId();
            Iterable<PesertaQuiz> pqTmp = pesertaQuizRepository.findByQuizIdAndIdUser(idQuiz,id);
            Iterator<PesertaQuiz> iter = pqTmp.iterator();
            if(iter.hasNext()){
                model.addAttribute("nilai",iter.next().getNilai());
            }
            else{
                model.addAttribute("nilai","Tidak Mengerjakan");
            }
            return "akses_quiz/end";
        }
        return "akses_quiz/start";
    }

    @GetMapping(path ="/Mulai")
    public String mulai(
            @RequestParam(value = "idQuiz")int idQuiz,
            Model model,
            RedirectAttributes redirectAttributes){
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        String name = authentication.getName();
        int id = userRepository.findByUsername(name).getUserId();
        String batasWaktu = aksesQuizService.getLimitTime(idQuiz);
        int pqid = aksesQuizService.createPQ(idQuiz,id);
        if(pqid == -1){
            Iterable<PesertaQuiz> pqTmp = pesertaQuizRepository.findByQuizIdAndIdUser(idQuiz,id);
            Iterator<PesertaQuiz> iter = pqTmp.iterator();
            model.addAttribute("nilai",iter.next().getNilai());
            return "akses_quiz/end";
        }
        redirectAttributes.addAttribute("pqid",pqid);
        redirectAttributes.addAttribute("batasWaktu",batasWaktu);
        redirectAttributes.addAttribute("idQuiz",idQuiz);
        return "redirect:/akses_quiz?idQuiz={idQuiz}&nomor=1&batasWaktu={batasWaktu}&pqid={pqid}";
    }


    @GetMapping(path = "/yakinKeluar")
    public String startYakinKeluarPage (
            @RequestParam(value = "idQuiz")int idQuiz,
            @RequestParam(value = "nomor")int nomor,
            @RequestParam(value = "batasWaktu")String batasWaktu,
            @RequestParam(value = "pqid")int pqid,
            Model model)throws Exception{
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        model.addAttribute("nomor",nomor);
        model.addAttribute("idQuiz",idQuiz);
        model.addAttribute("batasWaktu",batasWaktu);
        model.addAttribute("pqid",pqid);
        if(aksesQuizService.expired(batasWaktu,pqid)){
            aksesQuizService.setTruePeserta(pqid,idQuiz);
            String name = authentication.getName();
            int id = userRepository.findByUsername(name).getUserId();
            Iterable<PesertaQuiz> pqTmp = pesertaQuizRepository.findByQuizIdAndIdUser(idQuiz,id);
            Iterator<PesertaQuiz> iter = pqTmp.iterator();
            model.addAttribute("nilai",iter.next().getNilai());
            return "akses_quiz/end";
        }
        return "akses_quiz/yakinKeluar";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public void saveJawaban(
            @RequestParam("nomor") int nomor,
            @RequestParam("jawab") String jawab,
            @RequestParam("pqid") int pqid){
        aksesQuizService.savePQ(jawab,nomor,pqid);
    }

    @GetMapping(path = "/end")
    public String startEndingPage (Model model,
              @RequestParam(value = "idQuiz")int idQuiz,
              @RequestParam(value = "pqid")int pqid){
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        String name = authentication.getName();
        int id = userRepository.findByUsername(name).getUserId();
        Iterable<PesertaQuiz> pqTmp = pesertaQuizRepository.findByQuizIdAndIdUser(idQuiz,id);
        Iterator<PesertaQuiz> iter = pqTmp.iterator();
        aksesQuizService.setTruePeserta(pqid,idQuiz);
        model.addAttribute("nilai",iter.next().getNilai());
        return "akses_quiz/end";
    }


}
