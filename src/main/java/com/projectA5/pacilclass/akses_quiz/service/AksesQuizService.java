package com.projectA5.pacilclass.akses_quiz.service;

import com.projectA5.pacilclass.buat_quiz.model.Quiz;

import java.util.Date;
import java.util.List;

public interface AksesQuizService {
 String getSoal(int id,int nomor);

 int createPQ(int quizId,int id);

 int getTimelimit(int quizId);

 boolean dateCheck(int quizId);

 Date getStartDate(int quizId);

 Date getEndDate(int quizId);

 String getLimitTime(int quizId);

 boolean expired(String batasWaktu,int pqid) throws Exception;

 boolean lastQuestion(int quizId,int nomor) ;

 List<String> getJawabanBySoal(int questionNumber, int quizId);

 void savePQ(String jawaban,int nomor,int pqid);

 void  setTruePeserta(int idPq,int idQuiz);

 List<Quiz> getQuizFromKelas(int idKelas);

}
