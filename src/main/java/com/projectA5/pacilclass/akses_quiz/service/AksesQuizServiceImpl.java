package com.projectA5.pacilclass.akses_quiz.service;

import com.projectA5.pacilclass.administrasi_kelas.repository.KelasRepository;
import com.projectA5.pacilclass.akses_quiz.model.PesertaQuiz;
import com.projectA5.pacilclass.akses_quiz.repository.PesertaQuizRepository;
import com.projectA5.pacilclass.buat_quiz.model.Quiz;
import com.projectA5.pacilclass.buat_quiz.model.Pertanyaan;
import com.projectA5.pacilclass.buat_quiz.model.Jawaban;
import com.projectA5.pacilclass.buat_quiz.repository.JawabanRepository;
import com.projectA5.pacilclass.buat_quiz.repository.PertanyaanRepository;
import com.projectA5.pacilclass.buat_quiz.repository.QuizRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.projectA5.pacilclass.buat_quiz.service.PertanyaanService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Service
public class AksesQuizServiceImpl implements AksesQuizService {
    @Autowired
    private QuizRepository quizRepository;

    @Autowired
    private PesertaQuizRepository pesertaQuizRepository;

    @Autowired
    private KelasRepository kelasRepository;

    @Autowired
    private PertanyaanRepository pertanyaanRepository;

    @Autowired
    private  PertanyaanService pertanyaanService;

    @Autowired
    private JawabanRepository jawabanRepository;

    @Override
    public String getSoal(int id,int nomor){
        var quiztmp = quizRepository.findByQuizId(id);
        var pertanyaantmp = pertanyaanRepository.findByQuestionNumberAndQuiz(nomor,quiztmp);
        return pertanyaantmp.getQuestion();
    }


    @Override
    public int createPQ(int quizId,int idUser){
        var quiz = quizRepository.findByQuizId(quizId);
        var kuncitmp = "";
        Iterable<PesertaQuiz> pqTmp = pesertaQuizRepository.findByQuizIdAndIdUser(quizId,idUser);
        if (!pqTmp.iterator().hasNext()) {
            for (var a = 0; a < quiz.getPertanyaans().size(); a++) {
                kuncitmp += "X";
            }
            var returnPQ = new PesertaQuiz(0, kuncitmp, quizId,idUser);
            returnPQ.setIdUser(idUser);
            pesertaQuizRepository.save(returnPQ);
            return returnPQ.getPqid();
        }
        return -1;


    }

    @Override
    public int getTimelimit(int quizId){
        var quiztmp = quizRepository.findByQuizId(quizId);
        return quiztmp.getTimeLimit();
    }

    @Override
    public Date getStartDate(int quizId){
        var quiztmp = quizRepository.findByQuizId(quizId);
        return quiztmp.getStartDate();
    }

    @Override
    public Date getEndDate(int quizId){
        var quiztmp = quizRepository.findByQuizId(quizId);
        return quiztmp.getEndDate();
    }

    @Override
    public boolean dateCheck(int quizId){
        var currentTime = new Date();
        var startTime = this.getStartDate(quizId);
        var endTime = this.getEndDate(quizId);
        if(!startTime.after(currentTime) && !endTime.after(currentTime)){
            return true;
        }
        return false;
    }

    @Override
    public String getLimitTime(int quizId) {
        var now = Calendar.getInstance();
        long nowInSecs = now.getTimeInMillis();
        int limit = this.getTimelimit(quizId);
        var testSelesai = new Date(nowInSecs + (limit * 60 * 1000));
        var batasWaktuQuiz = this.getEndDate(quizId);
        if(testSelesai.after(batasWaktuQuiz)){
            testSelesai = batasWaktuQuiz;
        }
        var pattern = "MM/dd/yyyy HH:mm:ss";
        DateFormat df = new SimpleDateFormat(pattern);
        return df.format(testSelesai);
    }

    @Override
    public boolean expired(String batasWaktu,int pqid)throws Exception{
        var formatter=new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        var pesertaQuizTmp = findPesertaQuiz(pqid);
        var now = new Date();
        var batasWaktuDate = formatter.parse(batasWaktu);
        if(now.after(batasWaktuDate) || pesertaQuizTmp.getExpired()){
            return true;
        }
        return false;
    }

    @Override
    public List<String> getJawabanBySoal(int questionNumber, int quizId){
        var pertanyaan = pertanyaanService.getPertanyaanByNumber(questionNumber,quizId);
        Iterable<Jawaban> jawabanTmp = jawabanRepository.findByPertanyaan(pertanyaan);
        List<String> returnJawaban = new LinkedList<>();
        for(Jawaban j:jawabanTmp){
            returnJawaban.add(j.getTeksJawaban());
        }
        return returnJawaban;
    }

    @Override
    public void savePQ(String jawaban,int nomor,int pqid){
        var pq = findPesertaQuiz(pqid);
        if(!pq.getExpired()) {
            var jawabanString = pq.getKunci();
            var newJawaban = jawabanString.substring(0, nomor - 1) + jawaban + jawabanString.substring(nomor);
            pq.setKunci(newJawaban);
            pesertaQuizRepository.save(pq);
        }
    }

    @Override
    public boolean lastQuestion(int quizId,int nomor){
        var quiz = findQuiz(quizId);
        if(quiz.getPertanyaans().size()>nomor-1){
            return false;
        }
        return true;
    }

    public Quiz findQuiz(int quizId){
        return quizRepository.findByQuizId(quizId);
    }

    public PesertaQuiz findPesertaQuiz(int idPq){
        return pesertaQuizRepository.findByPqid(idPq);
    }

    @Override
    public void setTruePeserta(int idPq,int idQuiz){
        var pesertaTmp = findPesertaQuiz(idPq);
        pesertaTmp.setExpired(true);
        pesertaQuizRepository.save(pesertaTmp);
        setNilaiPeserta(pesertaTmp,findQuiz(idQuiz));
    }

    public void setNilaiPeserta(PesertaQuiz pesertaQuiz,Quiz quiz){
        String jawaban = pesertaQuiz.getKunci();
        var nilai = 0;
        List<Pertanyaan> pertanyaans = quiz.getPertanyaans();
        for(var a=0;a<jawaban.length();a++){
            if(pertanyaans.get(a).getAnswerKey()!= null) {
                if (jawaban.charAt(a) == pertanyaans.get(a).getAnswerKey().charAt(0)) {
                    nilai++;
                }
            }else {
                nilai++;
            }
        }
        pesertaQuiz.setNilai(nilai*100/jawaban.length());
        pesertaQuizRepository.save(pesertaQuiz);
    }

    // Untuk administrasi_kelas
    @Override
    public List<Quiz> getQuizFromKelas(int idKelas){
        return kelasRepository.findByid(idKelas).getQuizzes();
    }

}
