package com.projectA5.pacilclass.authentication.repository;

import com.projectA5.pacilclass.authentication.model.BaseUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<BaseUser, Integer> {
        BaseUser findByUserId(int userId);
        BaseUser findByUsername(String username);
}