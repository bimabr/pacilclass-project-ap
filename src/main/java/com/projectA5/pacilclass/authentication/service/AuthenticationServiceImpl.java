package com.projectA5.pacilclass.authentication.service;

import com.projectA5.pacilclass.authentication.model.BaseUser;
import com.projectA5.pacilclass.authentication.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationServiceImpl implements AuthenticationService{
    @Autowired
    private UserRepository userRepository;

    @Override
    public BaseUser createUser(BaseUser user) {
        userRepository.save(user);
        return user;
    }

    @Override
    public BaseUser getUserByid(int userId){
        return userRepository.findByUserId(userId);
    }

}