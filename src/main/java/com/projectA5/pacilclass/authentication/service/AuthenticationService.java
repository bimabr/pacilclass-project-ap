package com.projectA5.pacilclass.authentication.service;

import com.projectA5.pacilclass.authentication.model.BaseUser;

public interface AuthenticationService {
    BaseUser createUser(BaseUser user);
    BaseUser getUserByid(int userId);
}