package com.projectA5.pacilclass.authentication.model;

import com.projectA5.pacilclass.administrasi_kelas.model.Kelas;
import com.projectA5.pacilclass.announcement.model.Notification;
import com.projectA5.pacilclass.announcement.model.NotificationObserver;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "users")
@Data
public class BaseUser implements NotificationObserver {

    @Id
    @GeneratedValue
    @Column(name = "user_id")
    private int userId;

    @Column(name = "user_name", length = 50, nullable = false)
    private String username;

    @Column(name = "password", length = 255, nullable = false)
    private String password;

    @ManyToMany
    private List<Kelas> kelases = new ArrayList<>();

    @OneToMany(cascade = {CascadeType.ALL})
    private List<Notification> notifications = new ArrayList<>();

    public BaseUser(Integer userId, String username, String password){
        this.userId = userId;
        this.username = username;
        this.password = password;
    }

    public BaseUser() {

    }

    public String toString() {
        return "User{" + this.username + "}";
    }

    @Override
    public void addNotification(Notification notification) {
        this.notifications.add(notification);
    }
}