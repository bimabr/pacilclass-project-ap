package com.projectA5.pacilclass.authentication.controller;

import com.projectA5.pacilclass.authentication.model.BaseUser;
import com.projectA5.pacilclass.authentication.repository.UserRepository;
import com.projectA5.pacilclass.authentication.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/authentication")
public class AuthenticationController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping("/user")
    public String user() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication == null || authentication instanceof AnonymousAuthenticationToken) {
            return "authentication/login_page";
        }

        return "redirect:/";
    }

    @GetMapping("/authentication")
    public String addUser(Model model){
        model.addAttribute("user", new BaseUser());

        return "authentication/user_authentication";
    }

    @PostMapping("/sign_up")
    public String createUser(@ModelAttribute("user") BaseUser user){
        if (userRepository.findByUsername(user.getUsername()) != null) {
            return "redirect:/authentication/authentication";
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);

        System.out.println(user.getUsername());
        return "redirect:/authentication/user";
    }

    @GetMapping("/logout")
    public String logoutPage(HttpServletRequest request) {
        SecurityContextHolder.getContext().setAuthentication(null);
        HttpSession session = request.getSession(false);
        session.invalidate();
        return "redirect:/";
    }

}
