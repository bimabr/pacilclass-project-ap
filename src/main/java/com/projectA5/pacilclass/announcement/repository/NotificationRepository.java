package com.projectA5.pacilclass.announcement.repository;

import com.projectA5.pacilclass.announcement.model.Notification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NotificationRepository extends JpaRepository<Notification, Integer> {

}