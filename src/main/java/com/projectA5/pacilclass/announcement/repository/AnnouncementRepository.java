package com.projectA5.pacilclass.announcement.repository;

import com.projectA5.pacilclass.announcement.model.Announcement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnnouncementRepository extends JpaRepository<Announcement, Integer> {
    Iterable<Announcement> findByKelasId(int id);
}