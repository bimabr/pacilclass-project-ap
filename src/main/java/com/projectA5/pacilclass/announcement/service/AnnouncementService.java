package com.projectA5.pacilclass.announcement.service;

import com.projectA5.pacilclass.announcement.model.Announcement;


public interface AnnouncementService {
    Announcement createAnnouncement(Announcement kelas);
    Iterable<Announcement> getAllAnnouncements();
    Iterable<Announcement> getAnnouncementsByKelasId(int id);
}
