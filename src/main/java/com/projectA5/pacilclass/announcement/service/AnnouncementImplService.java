package com.projectA5.pacilclass.announcement.service;

import com.projectA5.pacilclass.announcement.model.Announcement;
import com.projectA5.pacilclass.announcement.repository.AnnouncementRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AnnouncementImplService implements AnnouncementService {
    @Autowired
    private AnnouncementRepository announcementRepository;

    @Override
    public Announcement createAnnouncement(Announcement announcement) {
        announcementRepository.save(announcement);
        return announcement;
    }

    @Override
    public Iterable<Announcement> getAllAnnouncements() {
        return announcementRepository.findAll();
    }

    @Override
    public Iterable<Announcement> getAnnouncementsByKelasId(int id) {
        return announcementRepository.findByKelasId(id);
    }
}
