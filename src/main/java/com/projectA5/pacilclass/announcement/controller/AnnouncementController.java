package com.projectA5.pacilclass.announcement.controller;

import com.projectA5.pacilclass.announcement.model.Announcement;
import com.projectA5.pacilclass.administrasi_kelas.model.Kelas;
import com.projectA5.pacilclass.authentication.model.BaseUser;
import com.projectA5.pacilclass.announcement.service.AnnouncementService;
import com.projectA5.pacilclass.announcement.repository.NotificationRepository;
import com.projectA5.pacilclass.administrasi_kelas.service.AdministrasiKelasService;
import com.projectA5.pacilclass.authentication.repository.UserRepository;
import com.projectA5.pacilclass.announcement.model.Notification;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping(path = "/announcement")
public class AnnouncementController {
    @Autowired
    private AnnouncementService announcementService;

    @Autowired
    private AdministrasiKelasService administrasiKelasService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private NotificationRepository notificationRepository;

    @GetMapping("")
    public String redirect() {
        return "redirect:/announcement/form";
    }

    @GetMapping("/form")
    public String getAnnouncementForm(Announcement announcement, Model model) {
        model.addAttribute("all_kelas", administrasiKelasService.getListKelas());

        return "announcement/announcement_form";
    }

    @PostMapping("/create_announcement")
    public String createAnnouncement(@Valid Announcement announcement, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "announcement/announcement_form";
        }

        announcementService.createAnnouncement(announcement);

        if (announcement.getKelasId() != null) {
            List<BaseUser> pesertas = administrasiKelasService.getKelasbyid(announcement.getKelasId()).getPesertas();
            for (BaseUser peserta : pesertas) {
                Notification notification = new Notification(peserta, announcement.getTitle());
                notification = notificationRepository.save(notification);
                peserta.addNotification(notification);
                userRepository.save(peserta);
            }
        }

        return "redirect:/administrasi_kelas/kelas?id=" + String.valueOf(announcement.getKelasId());
    }

    @GetMapping("/notification")
    public String getNotifications(HttpServletRequest request, Model model) {
        int userId = (int) request.getSession(false).getAttribute("userId");
        model.addAttribute("notifications", userRepository.findByUserId(userId).getNotifications());

        return "announcement/notification";
    }
}
