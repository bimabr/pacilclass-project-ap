package com.projectA5.pacilclass.announcement.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "Announcement")
@Data
public class Announcement {
    @Id
    @GeneratedValue
    @Column(name = "id", unique = true)
    private int id;

    @Column(name = "kelas_id", nullable = false)
    private Integer kelasId;

    @Column(name = "title", length = 25, nullable = false)
    private String title;

    @Column(name = "description", length = 100, nullable = false)
    private String description;

    public Integer getKelasId() {
        return this.kelasId;
    }

    public String getTitle() {
        return this.title;
    }
}
