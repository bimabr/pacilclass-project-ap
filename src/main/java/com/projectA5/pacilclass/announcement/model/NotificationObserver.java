package com.projectA5.pacilclass.announcement.model;

import com.projectA5.pacilclass.announcement.model.Notification;

public interface NotificationObserver {
    void addNotification(Notification notification);
}