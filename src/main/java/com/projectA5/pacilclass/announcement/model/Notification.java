package com.projectA5.pacilclass.announcement.model;

import com.projectA5.pacilclass.authentication.model.BaseUser;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "Notification")
@Data
public class Notification {
    @Id
    @GeneratedValue
    @Column(name = "id", unique = true)
    private int id;

    @Column(name = "text", length = 25, nullable = false)
    private String text;

    @ManyToOne
    private BaseUser user;

    public Notification() {

    }

    public Notification(BaseUser user, String text) {
        this.user = user;
        this.text = text;
    }
}
