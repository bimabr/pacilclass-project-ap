package com.projectA5.pacilclass.administrasi_kelas.model;

import com.projectA5.pacilclass.authentication.model.BaseUser;
import com.projectA5.pacilclass.buat_quiz.model.Quiz;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Kelas")
@Data

public class Kelas {
    @Id
    @GeneratedValue
    @Column(name = "id", unique = true)
    private int id;

    @Column(name = "lecture", nullable = false, length = 20)
    private String lecture;

    @Column(name = "class_name", length = 25, nullable = false, unique = false)
    private String name;

    @Column(name = "class_description", length = 30, nullable = true)
    private String description;

    @Column(name = "entryCode", length = 10, nullable = false, unique = true)
    private String entryCode;

    @OneToMany
    private  List<Quiz> quizzes = new ArrayList<>();

    @ManyToMany(mappedBy = "kelases")
    private List<BaseUser> pesertas = new ArrayList<>();

    @ManyToOne
    private BaseUser pengajars;


    public Kelas(Integer id, String lecture, String name, String description, String entryCode) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.entryCode = entryCode;
        this.lecture = lecture;
    }

    public Kelas() {

    }

}
