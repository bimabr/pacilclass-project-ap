package com.projectA5.pacilclass.administrasi_kelas.service;

import com.projectA5.pacilclass.administrasi_kelas.model.Kelas;
import com.projectA5.pacilclass.administrasi_kelas.repository.KelasRepository;
import com.projectA5.pacilclass.authentication.model.BaseUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdministrasiKelasImplservice implements AdministrasiKelasService {
    @Autowired
    private KelasRepository kelasRepository;


    @Override
    public Kelas createKelas(Kelas kelas) {
        kelasRepository.save(kelas);
        return kelas;
    }

    @Override
    public Kelas getKelasbyid(int id) {
        return kelasRepository.findByid(id);
    }



    @Override
    public Iterable<Kelas> getListKelas() {
        return kelasRepository.findAll();
    }

    @Override
    public Kelas updateKelas(String lecture, String entrycode, String description, String name, Kelas kelas) {
        kelas.setLecture(lecture);
        kelas.setEntryCode(entrycode);
        kelas.setDescription(description);
        kelas.setName(name);
        kelasRepository.save(kelas);
        return kelas;
    }

    @Override
    public boolean isInKelas(String name, Kelas kelas) {
        if (kelas.getPengajars().getUsername().equals(name)) {
            return true;
        }

        for (BaseUser peserta: kelas.getPesertas()
        ) {
            if (peserta.getUsername().equals(name)) {
                return true;
            }
        }

        return false;
    }


}
