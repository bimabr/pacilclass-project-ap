package com.projectA5.pacilclass.administrasi_kelas.service;
import com.projectA5.pacilclass.administrasi_kelas.model.Kelas;

public interface AdministrasiKelasService {
    Kelas createKelas(Kelas kelas);

    Kelas getKelasbyid(int id);

    Iterable<Kelas> getListKelas();

    Kelas updateKelas(String lecture, String entrycode, String description, String name, Kelas kelas);

    boolean isInKelas(String name, Kelas kelas);
}
