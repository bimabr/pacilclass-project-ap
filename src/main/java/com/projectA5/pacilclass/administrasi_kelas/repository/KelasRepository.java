package com.projectA5.pacilclass.administrasi_kelas.repository;
import com.projectA5.pacilclass.administrasi_kelas.model.Kelas;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KelasRepository extends JpaRepository<Kelas, Integer> {
    Kelas findByid(int id);

    Iterable<Kelas> findByPesertas_userId(int id);
}