package com.projectA5.pacilclass.administrasi_kelas.controller;

import com.projectA5.pacilclass.administrasi_kelas.model.Kelas;
import com.projectA5.pacilclass.administrasi_kelas.repository.KelasRepository;
import com.projectA5.pacilclass.administrasi_kelas.service.AdministrasiKelasService;
import com.projectA5.pacilclass.akses_quiz.service.AksesQuizService;
import com.projectA5.pacilclass.authentication.model.BaseUser;
import com.projectA5.pacilclass.authentication.repository.UserRepository;
import com.projectA5.pacilclass.announcement.service.AnnouncementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;


@Controller
@RequestMapping(path = "/administrasi_kelas")
public class AdministrasiKelasController {
    @Autowired
    private AdministrasiKelasService administrasiKelasService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private KelasRepository kelasRepository;

    @Autowired
    private AksesQuizService aksesQuizService;

    @Autowired
    private AnnouncementService announcementService;

    @GetMapping("")
    public String createKelas(Kelas kelas, HttpServletRequest request) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication == null || authentication instanceof AnonymousAuthenticationToken) {
            return "redirect:/authentication/user";
        }
        return "administrasi_kelas/register_form";
    }

    @PostMapping("/administrasi_kelas")
    public String addKelas(@Valid Kelas kelas, BindingResult result, Model model, HttpServletRequest request) {
        if (result.hasErrors()) {
            return "administrasi_kelas/register_form";
        }

        BaseUser user = userRepository.findByUserId(
                (int) request
                        .getSession(false)
                        .getAttribute("userId"));
        kelas.setPengajars(user);
        administrasiKelasService.createKelas(kelas);

        user.getKelases().add(kelas);
        userRepository.save(user);
        return "redirect:/administrasi_kelas/listkelas";
    }

    @GetMapping("/listkelas")
    public String getAll(Model model, HttpServletRequest request) {
        int id = (int) request
                .getSession(false)
                .getAttribute("userId");

        List<Kelas> kelases = (List<Kelas>) kelasRepository.findByPesertas_userId(id);
        model.addAttribute("userId", id);
        model.addAttribute("kelas", kelases);
        return "administrasi_kelas/listkelas";
    }

    @PostMapping("/deletekelas")
    public String deleteKelas(@RequestParam(name = "id") int id) {
        Kelas kelas = kelasRepository.findByid(id);

        for (BaseUser user: kelas.getPesertas()
        ) {
            user.getKelases().remove(kelas);
        }

        kelasRepository.deleteById(id);
        return "redirect:/administrasi_kelas/listkelas";
    }

    @PostMapping("/editkelas")
    public String editkelas(@Valid Kelas kelas, Model model, @RequestParam(name = "id") int id){
        return "administrasi_kelas/editkelas";
    }

    @PostMapping("/didalamkelas")
    public String didalamkelas(Model model, @RequestParam(name = "id")int id){
        return "redirect:/administrasi_kelas/kelas?id="+id;
    }

    @PostMapping("/updatekelas")
    public String updatekelas(
            @RequestParam(name = "id")int id,
            @RequestParam(name = "lecture")String lecture,
            @RequestParam(name = "entryCode")String entryCode,
            @RequestParam(name = "name")String name,
            @RequestParam(name = "description")String description){
        administrasiKelasService.updateKelas(lecture, entryCode, description, name, administrasiKelasService.getKelasbyid(id));
        return "redirect:/administrasi_kelas/listkelas";
    }


    @GetMapping("/kelas")
    public String kelas(Model model, @RequestParam(name = "id") int id, HttpServletRequest request) {
        int userId = (int) request
                .getSession(false)
                .getAttribute("userId");

        int idPengajar = kelasRepository.findByid(id).getPengajars().getUserId();

        model.addAttribute("userId", userId);
        model.addAttribute("id", id);
        model.addAttribute("idPengajar", idPengajar);

        //dibuat dalam akses_quiz
        System.out.println(aksesQuizService.getQuizFromKelas(id));
        model.addAttribute("quizes",aksesQuizService.getQuizFromKelas(id));

        model.addAttribute("announcements", announcementService.getAnnouncementsByKelasId(id));

        return "administrasi_kelas/didalamkelas";
    }

    @PostMapping("/buat_quiz")
    public String buatquiz(@RequestParam(name = "id") int id, RedirectAttributes attributes) {
        attributes.addAttribute("kelasId", id);
        return "redirect:/buat-quiz";
    }

    @PostMapping("/join")
    public String joinKelas(@RequestParam(value = "entry") String entryCode) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();

        for (Kelas kelas: kelasRepository.findAll()
        ) {
            if (kelas.getEntryCode().equals(entryCode)) {

                if (administrasiKelasService.isInKelas(username, kelas)) {
                    return "redirect:/";
                }

                BaseUser user = userRepository.findByUsername(username);
                user.getKelases().add(kelas);
                kelas.getPesertas().add(userRepository.findByUsername(username));
                userRepository.save(user);

                return "redirect:/administrasi_kelas/kelas?id=" + kelas.getId();
            }
        }
        return "redirect:/";
    }

}
