
var questionNumber = 1;
$(`.add-question`).on('click', function () {
    questionNumber += 1;
    addQuestion(questionNumber, false, null, null, null);
});

// When question is deleted
function qdelete(num) {

    if ($(`.a-question`).length == 1) {
        return;
    }

    $(`.q-${num}`).remove();

    // Change the number on other question that has number > num 
    $(`.a-question`).each(function (index, element) {

        if (index + 1 >= num) {
            let question = "";
            let checkedOption = "";
            let answerOptions = {a: "", b: "", c:"", d: ""};
            if ($(`#pertanyaan-${index + 2}`).length) {
                question = $(`#pertanyaan-${index + 2}`).val();
                checkedOption = $(`input[name=answers-${index + 2}]:checked`).val();
    
                answerOptions = {
                    a : $(`#A-${index + 2}`).val(),
                    b : $(`#B-${index + 2}`).val(),
                    c : $(`#C-${index + 2}`).val(),
                    d : $(`#D-${index + 2}`).val(),
                };
            }

            $(element).remove();
            addQuestion(index + 1, true, question, checkedOption, answerOptions);
        }
    });

    questionNumber--;
};

function addQuestion(questionNumber, isDelete, prevQuestion, prevCheckedAnswer, answerOptions) {
    $(`
    <div class="col-12 a-question q-${questionNumber} mb-3">
        <div class="d-flex justify-content-between mb-2 q-title">
        <p class="mb-1 font-weight-bolder">Question ${questionNumber}</p>
        <button class="btn btn-danger q-del" id="q-delete-${questionNumber}" onclick="qdelete(${questionNumber})"><p>Delete</p></button>
        </div>
        <div class="q-box mb-3">
            <textarea name="question" id="pertanyaan-${questionNumber}" cols="30" rows="10"></textarea>
        </div>
    <div class="q-answers">
    <div class="d-flex">
        <div class="q-single-ans col-6 d-flex align-items-center">
            <input type="radio" name="answers-${questionNumber}" id="answer-A-${questionNumber}" value="A">
            <div class="ml-1"></div>
            <label for="answer-A-${questionNumber}">A</label>
            <div class="ml-1"></div>
            <input type="text" id="A-${questionNumber}" >
        </div>
    <div class="q-single-ans col-6 d-flex align-items-center">
                    <input type="radio" name="answers-${questionNumber}" id="answer-C-${questionNumber}" value="C">
                    <div class="ml-1"></div>
                    <label for="answer-C-${questionNumber}">C</label>
                    <div class="ml-1"></div>
                    <input type="text" id="C-${questionNumber}" >
                    </div>
                    </div>
                    <div class="mb-2"></div>
            <div class="d-flex">
            <div class="q-single-ans col-6 d-flex align-items-center">
            <input type="radio" name="answers-${questionNumber}" id="answer-B-${questionNumber}" value="B">
            <div class="ml-1"></div>
                    <label for="answer-B-${questionNumber}">B</label>
                    <div class="ml-1"></div>
                    <input type="text" id="B-${questionNumber}" >
                    </div>
                    
                    <div class="q-single-ans col-6 d-flex align-items-center">
                    <input type="radio" name="answers-${questionNumber}" id="answer-D-${questionNumber}" value="D">
                    <div class="ml-1"></div>
                    <label for="answer-D-${questionNumber}">D</label>
                    <div class="ml-1"></div>
                    <input type="text" id="D-${questionNumber}" >
                    </div>
                    </div>
        </div>
        
    </div>
    `).insertBefore(`.plus-button`);

    if (isDelete) {
        console.log(`${prevQuestion}`);
        $(`#pertanyaan-${questionNumber}`).val(prevQuestion);
        $(`#answer-${prevCheckedAnswer}-${questionNumber}`).attr('checked', true);
        $(`#A-${questionNumber}`).val(answerOptions.a);
        $(`#B-${questionNumber}`).val(answerOptions.b);
        $(`#C-${questionNumber}`).val(answerOptions.c);
        $(`#D-${questionNumber}`).val(answerOptions.d);
    }
}

$(`.btn-save`).on('click', function () {

    var lst = [];

    $(`.a-question`).each(function (index, element) {

        var question = $(`#pertanyaan-${index + 1}`).val();

        if (question.length !== 0) {
            var qObj = {
                questionNumber: index + 1,
                question: question,
                answerKey: $(`.q-${index + 1} input[name=answers-${index + 1}]:checked`).val(),
                answers: [
                    {
                        teksJawaban: $(`#A-${index + 1}`).val(),
                        opsiJawaban: "A"
                    },
                    {
                        teksJawaban: $(`#B-${index + 1}`).val(),
                        opsiJawaban: "B"
                    },
                    {
                        teksJawaban: $(`#C-${index + 1}`).val(),
                        opsiJawaban: "C"
                    },
                    {
                        teksJawaban: $(`#D-${index + 1}`).val(),
                        opsiJawaban: "D"
                    }
                ]
            }

            lst.push(qObj);
        }


    });

    var data = {
        quizName: $(`#quiz-name`).val(),
        timeLimit: $(`#time-limit`).val(),
        startDate: retrieveDateSelect("start"),
        endDate: retrieveDateSelect("end"),
        questions: lst
    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/buat-quiz/save?id=" + $(this).attr("data-kelas"),
        data: JSON.stringify(data),
        success: function (response) {
            $(`.modal-body`).text(`${data.quizName} has been saved`);
            $(`#exampleModal`).modal({
                keyboard: false,
                backdrop: 'static'
            });
        },
        complete: function(response) {
            let time = setTimeout(function() {
                document.location.href = response.responseJSON.url;
                document.clearTimeout(time);
            }, 3000);
        }
    });
});

$(`.btn-back`).on('click', function () {
    $(`.buat-pertanyaan-form`).hide(400); 
    $(`.buat-quiz-form`).show(300);
});