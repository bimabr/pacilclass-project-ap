
var d = new Date();
$(`.buat-pertanyaan-form`).hide();
$(`select[name=end-time-min]`).val(5);

$.fn.inputFilter = function(inputFilter) {
  return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
    if (inputFilter(this.value)) {
      this.oldValue = this.value;
      this.oldSelectionStart = this.selectionStart;
      this.oldSelectionEnd = this.selectionEnd;
    } else if (this.hasOwnProperty("oldValue")) {
      this.value = this.oldValue;
      this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
    } else {
      this.value = "";
    }
  });
};


$("#time-limit").inputFilter(function (value) {
    return /^\d*$/.test(value); 
});

$(`select[name=start-time-min]`).on('change', function () {
  $(`select[name=end-time-min]`).val(parseInt(this.value) + 5);
});

$(`select[name="start-time-hour"]`).on('change', function () {
  $(`select[name=end-time-hour]`).val(this.value);
});    

function retrieveDateSelect(dateName) { 
  var month = $(`select[name="${dateName}-month"]`).val();
  var date = $(`select[name="${dateName}-date"]`).val();
  var hour = $(`select[name="${dateName}-time-hour"]`).val();
  var minute = $(`select[name="${dateName}-time-min"]`).val();
  
  var year = d.getFullYear();

  month = month < 10 ? '0'+month : month.toString();
  date = date < 10 ? '0'+date : date.toString();
  hour = hour < 10 ? '0'+hour : hour.toString();
  minute = minute < 10 ? '0'+minute : minute.toString();

  var datetime = `${year}-${month}-${date}T${hour}:${minute}`;
  return datetime;
};

// On submit Buat Quiz Form 
$(`.buat-quiz-form form`).submit(function (e) { 
  e.preventDefault();
  
  var data = {
    quizName: $(`#quiz-name`).val(),
    timeLimit: $(`#time-limit`).val(),
    startDate: retrieveDateSelect("start"),
    endDate: retrieveDateSelect("end"),
  };


  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: "/buat-quiz/validate",
    data: JSON.stringify(data),
    dataType: "json",
    success: function (response) {
      $(`.buat-quiz-form`).hide(300)
      $(`.buat-pertanyaan-form`).show(400);
    },
    error: function (xhr, status, error) {
      $(`#end-date-row`).attr('data-toggle', 'tooltip');
      $(`#end-date-row`).attr('data-placement', 'bottom');
      $(`#end-date-row`).attr('title', 'Invalid Date');
      $(`#end-date-row`).attr('style', 'border: 1px solid red');
      $(`#end-date-row`).tooltip('show');
    }
  });

});


  