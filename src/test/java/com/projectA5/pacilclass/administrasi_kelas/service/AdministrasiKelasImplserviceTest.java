package com.projectA5.pacilclass.administrasi_kelas.service;

import com.projectA5.pacilclass.administrasi_kelas.model.Kelas;
import com.projectA5.pacilclass.administrasi_kelas.repository.KelasRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)

class AdministrasiKelasImplserviceTest {
    @Mock
    private KelasRepository kelasRepository;

    @InjectMocks
    private AdministrasiKelasImplservice administrasi_kelas_service;

    private Kelas kelas;

    @BeforeEach
    public void setUp(){
        kelas = new Kelas();
        kelas.setLecture("pak rms");
        kelas.setName("Robby");
        kelas.setId(12345);
        kelas.setDescription("ini kelas adpro");
        kelas.setEntryCode("1234");
    }

    @Test
    void testServiceCreateKelas(){
        Kelas kelas = new Kelas();
        assertNull(kelas.getDescription());
        lenient().when(administrasi_kelas_service.createKelas(kelas)).thenReturn(kelas);
    }


    @Test
    void testServiceGetkelasByid(){
        lenient().when(administrasi_kelas_service.getKelasbyid(12345)).thenReturn(kelas);
        Kelas resultkelas = administrasi_kelas_service.getKelasbyid(kelas.getId());
        assertEquals(kelas.getId(), resultkelas.getId());
    }

    @Test
    void testServiceUpdateKelas(){
        lenient().when(administrasi_kelas_service.getKelasbyid(12345)).thenReturn(kelas);
        Kelas resultkelas = administrasi_kelas_service.getKelasbyid(kelas.getId());
        administrasi_kelas_service.updateKelas("indonesia", "abcd", "abc", "name", resultkelas);
        assertEquals("indonesia", resultkelas.getLecture());
        assertEquals("abcd", resultkelas.getEntryCode());
        assertEquals("abc", resultkelas.getDescription());
    }

    @Test
    void testServiceListKelas(){
        lenient().when(administrasi_kelas_service.getKelasbyid(12345)).thenReturn(kelas);

    }

}
