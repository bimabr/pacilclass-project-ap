package com.projectA5.pacilclass.administrasi_kelas.controller;

import com.projectA5.pacilclass.administrasi_kelas.repository.KelasRepository;
import com.projectA5.pacilclass.administrasi_kelas.model.Kelas;
import com.projectA5.pacilclass.administrasi_kelas.service.AdministrasiKelasImplservice;
import com.projectA5.pacilclass.akses_quiz.service.AksesQuizServiceImpl;
import com.projectA5.pacilclass.authentication.model.BaseUser;
import com.projectA5.pacilclass.authentication.repository.UserRepository;
import com.projectA5.pacilclass.authentication.service.AuthenticationService;
import com.projectA5.pacilclass.authentication.service.AuthenticationServiceImpl;
import com.projectA5.pacilclass.authentication.service.MyUserDetailsService;
import com.projectA5.pacilclass.announcement.service.AnnouncementService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.mockito.Mockito.when;





@WebMvcTest(controllers = AdministrasiKelasController.class)
public class AdministrasiKelasControllerTest {
    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private AksesQuizServiceImpl aksesQuizService;

    @MockBean
    private AdministrasiKelasImplservice administrasiKelasService;

    @MockBean
    private KelasRepository kelasRepository;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private MyUserDetailsService userDetailsService;

    @MockBean
    private AuthenticationService authenticationService;

    @MockBean
    private AuthenticationServiceImpl authenticationServiceImpl;

    @MockBean
    private AnnouncementService announcementService;

    @MockBean
    private Kelas kelas;

    private MockMvc mockMvc;

    private BaseUser user;


    @BeforeEach
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).apply(springSecurity()).build();
    }

    @Test
    @WithMockUser(username = "budi",roles = {"USER"})
    void testAccesurlAdiministrasiKelas() throws Exception {
        when(userRepository.findByUsername("budi")).thenReturn(new BaseUser(1, "budi", "a"));

        mockMvc.perform(get("/administrasi_kelas"))
                .andExpect(view().name("administrasi_kelas/register_form"));
    }

   /* @Test
    @WithMockUser(roles = {"USER"})
    void whenUpdateKelasURLIsAccessed()throws Exception {
        when(userRepository.findByUsername("budi")).thenReturn(new BaseUser(1, "budi", "a"));
        mockMvc.perform(get("/listkelas"))
                .andExpect(view().name("administrasi_kelas/listkelas"));
    }
*/
    /*@Test
    @WithMockUser(username = "budi", roles = {"USER"})
    void whenListKelasURLIsAccessed()throws Exception {
        when(userRepository.findByUsername("budi")).thenReturn(new BaseUser(1, "budi", "a"));
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(authentication.getName()).thenReturn("user");
        lenient().when(userRepository.findByUsername("user")).thenReturn(user);
        System.out.println(mockMvc.perform(get("/administrasi_kelas/listkelas")));
    }

    @Test
    @WithMockUser(username = "budi", roles = {"USER"})
    void whenDidalamKelasURLIsAccessed()throws Exception{
        when(userRepository.findByUsername("budi")).thenReturn(new BaseUser(1, "budi", "a"));
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(authentication.getName()).thenReturn("user");
        lenient().when(userRepository.findByUsername("user")).thenReturn(user);
        mockMvc.perform(get("/administrasi_kelas/didalamkelas")
                .param("id", "1"))
                .andExpect(handler().methodName("didalamkelas"));


    }
*/

}
