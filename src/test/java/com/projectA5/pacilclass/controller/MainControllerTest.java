package com.projectA5.pacilclass.controller;

import com.projectA5.pacilclass.authentication.model.BaseUser;
import com.projectA5.pacilclass.authentication.repository.UserRepository;
import com.projectA5.pacilclass.authentication.service.MyUserDetailsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.http.HttpSession;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(controllers = MainController.class)
class MainControllerTest {
    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private MyUserDetailsService userDetailsService;

    @BeforeEach
    public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).apply(springSecurity()).build();
    }

    @Test
    public void testLandingPage() throws Exception {
        HttpSession httpSession = mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("landing_page"))
                .andReturn()
                .getRequest()
                .getSession();

        assertNull(httpSession.getAttribute("userId"));
    }

    @Test
    @WithMockUser(username = "budi")
    public void testLandingPageWithLoggedUser() throws Exception {
        when(userRepository.findByUsername("budi")).thenReturn(new BaseUser(1, "budi", "1"));

        HttpSession httpSession = mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("landing_page"))
                .andReturn()
                .getRequest()
                .getSession();

        assertEquals(httpSession.getAttribute("userId"), 1);
    }
}