package com.projectA5.pacilclass.authentication.controller;

import com.projectA5.pacilclass.authentication.model.BaseUser;
import com.projectA5.pacilclass.authentication.service.AuthenticationServiceImpl;
import com.projectA5.pacilclass.authentication.repository.UserRepository;
import com.projectA5.pacilclass.authentication.service.MyUserDetailsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.Mockito.lenient;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = AuthenticationController.class)
class AuthenticationControllerTest {
    @MockBean
    private UserRepository userRepository;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private MyUserDetailsService userDetailsService;

    @MockBean
    private AuthenticationServiceImpl authenticationService;

    private MockMvc mockMvc;

    private BaseUser user;

    @BeforeEach
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(this.webApplicationContext)
                .apply(springSecurity())
                .build();
        user = new BaseUser(123,"bambang","pass");
    }

    @Test
    void accessAuthentication() throws Exception {
        mockMvc.perform(get("/authentication/authentication"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("user"))
                .andExpect(view().name("authentication/user_authentication"));
    }

    @Test
    void accessSignUp() throws Exception {
        BaseUser testUser = new BaseUser(1, "budi", "kunti");
        mockMvc.perform(post("/authentication/sign_up")
                .flashAttr("user", testUser));
    }

    @Test
    void accessUserNotLoginYet() throws Exception {
        mockMvc.perform(get("/authentication/user"))
                .andExpect(status().isOk())
                .andExpect(view().name("authentication/login_page"));
    }

    @Test
    @WithMockUser(username = "bambang",roles = {"USER"})
    void testUserAlreadyLogin() throws Exception {
        lenient().when(userRepository.findByUsername("bambang")).thenReturn(new BaseUser(123, "bambang", "pass"));
        mockMvc.perform(get("/authentication/user"))
                .andExpect(redirectedUrl("/"));
    }

    @Test
    @WithMockUser(username = "bambang",roles = {"USER"})
    void testUserLogout() throws Exception {
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(authentication.getName()).thenReturn("user");
        lenient().when(userRepository.findByUsername("user")).thenReturn(user);
        mockMvc.perform(get("/authentication/logout"))
                .andExpect(redirectedUrl("/"));
    }
}
