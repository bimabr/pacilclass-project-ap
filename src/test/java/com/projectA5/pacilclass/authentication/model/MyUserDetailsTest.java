package com.projectA5.pacilclass.authentication.model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

class MyUserDetailsTest {
    @Test
    void testGetUsername(){
        BaseUser user = new BaseUser();
        user.setUserId(123);
        user.setUsername("Bambang");
        user.setPassword("password");
        MyUserDetails userDetails = new MyUserDetails(user);
        assertEquals("Bambang", userDetails.getUsername());
    }

    @Test
    void testGetPassword(){
        BaseUser user = new BaseUser();
        user.setUserId(123);
        user.setUsername("Bambang");
        user.setPassword("password");
        MyUserDetails userDetails = new MyUserDetails(user);
        assertEquals("password", userDetails.getPassword());
    }

    @Test
    void testIsAccountNonExpired(){
        BaseUser user = new BaseUser();
        user.setUserId(123);
        user.setUsername("Bambang");
        user.setPassword("password");
        MyUserDetails userDetails = new MyUserDetails(user);
        assertEquals(true, userDetails.isAccountNonExpired());
    }

    @Test
    void testIsAccountNonLocked(){
        BaseUser user = new BaseUser();
        user.setUserId(123);
        user.setUsername("Bambang");
        user.setPassword("password");
        MyUserDetails userDetails = new MyUserDetails(user);
        assertEquals(true, userDetails.isAccountNonLocked());
    }

    @Test
    void testIsCredentialNonExpired(){
        BaseUser user = new BaseUser();
        user.setUserId(123);
        user.setUsername("Bambang");
        user.setPassword("password");
        MyUserDetails userDetails = new MyUserDetails(user);
        assertEquals(true, userDetails.isCredentialsNonExpired());
    }

    @Test
    void testIsEnabled(){
        BaseUser user = new BaseUser();
        user.setUserId(123);
        user.setUsername("Bambang");
        user.setPassword("password");
        MyUserDetails userDetails = new MyUserDetails(user);
        assertEquals(true, userDetails.isEnabled());
    }
}
