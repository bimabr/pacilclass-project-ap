package com.projectA5.pacilclass.authentication.model;

import com.projectA5.pacilclass.administrasi_kelas.model.Kelas;
import com.projectA5.pacilclass.authentication.model.BaseUser;
import com.projectA5.pacilclass.authentication.repository.UserRepository;
import com.projectA5.pacilclass.authentication.service.AuthenticationServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class BaseUserTest {
    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private AuthenticationServiceImpl authenticationService;

    private BaseUser user;

    @BeforeEach
    public void setUp() {
        user = new BaseUser();
        user.setUserId(123);
        user.setUsername("Bambang");
        user.setPassword("password");
    }

    @Test
    void testServiceGetUserById(){
        lenient().when(authenticationService.getUserByid(123)).thenReturn(user);
        BaseUser userResult = authenticationService.getUserByid(user.getUserId());
        assertEquals(user.getUserId(), userResult.getUserId());
    }

    @Test
    void testToString(){
        String expected = "User{Bambang}";
        assertEquals(expected, user.toString());
    }
}
