package com.projectA5.pacilclass.akses_quiz.controller;

import com.projectA5.pacilclass.akses_quiz.model.PesertaQuiz;
import com.projectA5.pacilclass.akses_quiz.repository.PesertaQuizRepository;
import com.projectA5.pacilclass.akses_quiz.service.AksesQuizServiceImpl;
import com.projectA5.pacilclass.authentication.model.BaseUser;
import com.projectA5.pacilclass.authentication.repository.UserRepository;
import com.projectA5.pacilclass.authentication.service.MyUserDetailsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = AksesQuizController.class)
public class AksesQuizControllerTest {
    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @MockBean
    private MyUserDetailsService userDetailsService;


    @MockBean
    private AksesQuizServiceImpl aksesQuizService;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private PesertaQuizRepository pesertaQuizRepository;

    private BaseUser user;

    private PesertaQuiz pesertaQuiz;

    @BeforeEach
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).apply(springSecurity()).build();
        user = new BaseUser(1,"A","A");
        pesertaQuiz = new PesertaQuiz(1,"A",0,1);
    }

    @Test
    @WithMockUser(roles = {"USER"})
    void whenAksesQuizURLIsAccessed() throws Exception {
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        mockMvc.perform(get("/akses_quiz")
                .param("idQuiz","1")
                .param("nomor","1")
                .param("batasWaktu","05%2F20%2F2021%2019:31:24")
                .param("pqid","1"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("keluarinQuiz"))
                .andExpect(model().attributeExists("nomor"))
                .andExpect(model().attributeExists("idQuiz"))
                .andExpect(model().attributeExists("batasWaktu"))
                .andExpect(view().name("akses_quiz/tampilan_quiz"));
        verify(aksesQuizService, times(1)).getSoal(1,1);

    }

    @Test
    @WithMockUser(roles = {"USER"})
    void whenAksesQuizUREndLIsAccessed() throws Exception {
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(authentication.getName()).thenReturn("user");
        lenient().when(userRepository.findByUsername("user")).thenReturn(user);
        List<PesertaQuiz> pqTmp = new ArrayList<PesertaQuiz>();
        pqTmp.add(pesertaQuiz);
        Iterable<PesertaQuiz> iter = pqTmp;
        lenient().when(pesertaQuizRepository.findByQuizIdAndIdUser(1,1)).thenReturn(pqTmp);
        SecurityContextHolder.setContext(securityContext);
        lenient().when(aksesQuizService.lastQuestion(1,1)).thenReturn(true);
        mockMvc.perform(get("/akses_quiz")
                .param("idQuiz","1")
                .param("nomor","1")
                .param("batasWaktu","05%2F20%2F2021%2019:31:24")
                .param("pqid","1"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("keluarinQuiz"))
                .andExpect(view().name("akses_quiz/end"));
    }


   @Test
   @WithMockUser(roles = {"USER"})
    void whenStartUrlIsAccesed() throws Exception {
       Authentication authentication = Mockito.mock(Authentication.class);
       SecurityContext securityContext = Mockito.mock(SecurityContext.class);
       Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
       Mockito.when(authentication.getName()).thenReturn("user");
        mockMvc.perform(get("/akses_quiz/start")
                .param("idQuiz", "1")
                .param("idUser","1"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("idQuiz"))
                .andExpect(view().name("akses_quiz/start"));
        verify(aksesQuizService, times(1)).dateCheck( 1);
    }

    @Test
    @WithMockUser(roles = {"USER"})
    void whenStartUrlExpiredIsAccesed() throws Exception {
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(authentication.getName()).thenReturn("user");
        lenient().when(userRepository.findByUsername("user")).thenReturn(user);
        List<PesertaQuiz> pqTmp = new ArrayList<PesertaQuiz>();
        pqTmp.add(pesertaQuiz);
        Iterable<PesertaQuiz> iter = pqTmp;
        lenient().when(pesertaQuizRepository.findByQuizIdAndIdUser(1,1)).thenReturn(pqTmp);
        SecurityContextHolder.setContext(securityContext);
        lenient().when(aksesQuizService.dateCheck(1)).thenReturn(true);
        mockMvc.perform(get("/akses_quiz/start")
                .param("idQuiz", "1")
                .param("idUser","1"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("idQuiz"))
                .andExpect(view().name("akses_quiz/end"));
        verify(aksesQuizService, times(1)).dateCheck( 1);
    }

    @Test
    @WithMockUser(roles = {"USER"})
    void whenMulaiURLIsAccessed() throws Exception {
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(authentication.getName()).thenReturn("user");
        lenient().when(userRepository.findByUsername("user")).thenReturn(user);
        SecurityContextHolder.setContext(securityContext);
        lenient().when(aksesQuizService.getLimitTime(1)).thenReturn("05%2F20%2F2021%2019:31:24");
        mockMvc.perform(get("/akses_quiz/Mulai")
                .param("idQuiz","1"))
                .andExpect(redirectedUrl("/akses_quiz?idQuiz=1&nomor=1&batasWaktu=05%252F20%252F2021%252019:31:24&pqid=0"));
    }

    @Test
    @WithMockUser(roles = {"USER"})
    void whenMulaiURLAlreadyIsAccessed() throws Exception {
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(authentication.getName()).thenReturn("user");
        lenient().when(userRepository.findByUsername("user")).thenReturn(user);
        List<PesertaQuiz> pqTmp = new ArrayList<PesertaQuiz>();
        pqTmp.add(pesertaQuiz);
        Iterable<PesertaQuiz> iter = pqTmp;
        lenient().when(pesertaQuizRepository.findByQuizIdAndIdUser(1,1)).thenReturn(pqTmp);
        lenient().when(aksesQuizService.createPQ(1,1)).thenReturn(-1);
        lenient().when(aksesQuizService.getLimitTime(1)).thenReturn("05%2F20%2F2021%2019:31:24");
        mockMvc.perform(get("/akses_quiz/Mulai")
                .param("idQuiz","1"))
                .andExpect(view().name("akses_quiz/end"));
    }

    @Test
    @WithMockUser(roles = {"USER"})
    void whenYakinKeluarURLIsAccessed() throws Exception {
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(authentication.getName()).thenReturn("user");
        lenient().when(userRepository.findByUsername("user")).thenReturn(user);
        mockMvc.perform(get("/akses_quiz/yakinKeluar")
                .param("idQuiz","1")
                .param("nomor","1")
                .param("batasWaktu","05%2F20%2F2021%2019:31:24")
                .param("pqid","1"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("startYakinKeluarPage"))
                .andExpect(model().attributeExists("nomor"))
                .andExpect(model().attributeExists("idQuiz"))
                .andExpect(model().attributeExists("batasWaktu"))
                .andExpect(view().name("akses_quiz/yakinKeluar"));
    }

    @Test
    @WithMockUser(roles = {"USER"})
    void whenYakinKeluarURLExpiredIsAccessed() throws Exception {
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        lenient().when(userRepository.findByUsername("user")).thenReturn(user);
        List<PesertaQuiz> pqTmp = new ArrayList<PesertaQuiz>();
        pqTmp.add(pesertaQuiz);
        Iterable<PesertaQuiz> iter = pqTmp;
        lenient().when(pesertaQuizRepository.findByQuizIdAndIdUser(1,1)).thenReturn(pqTmp);
        lenient().when(aksesQuizService.expired("05%2F20%2F2021%2019:31:22",1)).thenReturn(true);
        mockMvc.perform(get("/akses_quiz/yakinKeluar")
                .param("idQuiz","1")
                .param("nomor","1")
                .param("batasWaktu","05%2F20%2F2021%2019:31:22")
                .param("pqid","1"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("nomor"))
                .andExpect(model().attributeExists("idQuiz"))
                .andExpect(model().attributeExists("batasWaktu"))
                .andExpect(view().name("akses_quiz/end"));
    }

    @Test
    @WithMockUser(roles = {"USER"})
     void whenSaveJawabanIsAccesed() throws Exception {
        mockMvc.perform(post("/akses_quiz/save")
                .accept(MediaType.ALL)
                .param("nomor","1")
                .param("jawab","A")
                .param("pqid","1"))
                .andExpect(handler().methodName("saveJawaban"));
    }

    @Test
    @WithMockUser(roles = {"USER"})
    void whenEndLIsAccessed() throws Exception {
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(authentication.getName()).thenReturn("user");
        lenient().when(userRepository.findByUsername("user")).thenReturn(user);
        List<PesertaQuiz> pqTmp = new ArrayList<PesertaQuiz>();
        pqTmp.add(pesertaQuiz);
        Iterable<PesertaQuiz> iter = pqTmp;
        lenient().when(pesertaQuizRepository.findByQuizIdAndIdUser(1,1)).thenReturn(pqTmp);
        SecurityContextHolder.setContext(securityContext);
        mockMvc.perform(get("/akses_quiz/end")
                .param("idQuiz","1")
                .param("pqid","1"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("startEndingPage"))
                .andExpect(view().name("akses_quiz/end"));
    }
}
