package com.projectA5.pacilclass.akses_quiz.service;
import com.projectA5.pacilclass.administrasi_kelas.model.Kelas;
import com.projectA5.pacilclass.administrasi_kelas.repository.KelasRepository;
import com.projectA5.pacilclass.akses_quiz.model.PesertaQuiz;
import com.projectA5.pacilclass.akses_quiz.repository.PesertaQuizRepository;
import com.projectA5.pacilclass.authentication.model.BaseUser;
import com.projectA5.pacilclass.buat_quiz.model.Jawaban;
import com.projectA5.pacilclass.buat_quiz.model.Pertanyaan;
import com.projectA5.pacilclass.buat_quiz.model.Quiz;
import com.projectA5.pacilclass.buat_quiz.repository.JawabanRepository;
import com.projectA5.pacilclass.buat_quiz.repository.PertanyaanRepository;
import com.projectA5.pacilclass.buat_quiz.repository.QuizRepository;
import com.projectA5.pacilclass.buat_quiz.service.PertanyaanService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AksesKelasImplserviceTest {
    @Mock
    private QuizRepository quizRepository;

    @Mock
    private PertanyaanRepository pertanyaanRepository;

    @Mock
    private PesertaQuizRepository pesertaQuizRepository;

    @Mock
    private JawabanRepository jawabanRepository;

    @Mock
    private KelasRepository kelasRepository;


    @Mock
    private PertanyaanService pertanyaanService;

    @InjectMocks
    private AksesQuizServiceImpl aksesQuizService;


    private Quiz quiz;

    private Pertanyaan pertanyaan;

    private PesertaQuiz pesertaQuiz;

    private Jawaban jawaban;

    private BaseUser user;

    private Kelas kelas;
    @BeforeEach
    public void setUp() throws ParseException {
        user = new BaseUser(1,"A","A");
        quiz = new Quiz();
        quiz.setQuizId(2);
        quiz.setQuizName("test");
        List<Pertanyaan> pertanyaans =new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy'T'HH:mm");
        quiz.setStartDate(sdf.parse("10-10-2012T10:10"));
        quiz.setEndDate(sdf.parse("10-10-2012T11:11"));
        quiz.setTimeLimit(1);
        pertanyaan = new Pertanyaan();
        pertanyaan.setPertanyaanId(2);
        pertanyaan.setQuestionNumber(2);
        pertanyaan.setQuiz(quiz);
        pertanyaan.setAnswerKey("A");
        pertanyaan.setQuestion("1+1?");
        pertanyaans.add(pertanyaan);
        quiz.setPertanyaans(pertanyaans);
        pesertaQuiz = new PesertaQuiz(1,"A",0,1);
        pesertaQuiz.setPqid(1);
        pesertaQuiz.setNilai(0);
        pesertaQuiz.setKunci("A");
        jawaban = new Jawaban();
        jawaban.setJawabanId(1);
        jawaban.setOpsiJawaban("A");
        jawaban.setTeksJawaban("Pulau Sumatra");
        jawaban.setPertanyaan(pertanyaan);
        pesertaQuiz.setQuizId(2);
        kelas = new Kelas(12345,"pak rms","Michael","adpro","1234");
        List<Quiz> quizTmp = new ArrayList<>();
        quizTmp.add(quiz);
        kelas.setQuizzes(quizTmp);
    }

    @Test
    void testGetSoal(){
        lenient().when(quizRepository.findByQuizId(2)).thenReturn(quiz);
        lenient().when(pertanyaanRepository.findByQuestionNumberAndQuiz(2,quiz)).thenReturn(pertanyaan);
        String Test = aksesQuizService.getSoal(2,2);
        assertEquals("1+1?",Test);
    }

    @Test
    void testGetTimeLimit() throws ParseException{
        lenient().when(quizRepository.findByQuizId(2)).thenReturn(quiz);
        int Test = aksesQuizService.getTimelimit(2);
        assertEquals(1,Test);
    }

    @Test
    void testGetStartDate() throws ParseException{
        lenient().when(quizRepository.findByQuizId(2)).thenReturn(quiz);
        Date Test = aksesQuizService.getStartDate(2);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy'T'HH:mm");
        assertEquals(Test,sdf.parse("10-10-2012T10:10"));
    }

    @Test
    void testGetEndDate() throws ParseException{
        lenient().when(quizRepository.findByQuizId(2)).thenReturn(quiz);
        Date Test = aksesQuizService.getEndDate(2);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy'T'HH:mm");
        assertEquals(Test,sdf.parse("10-10-2012T11:11"));
    }

    @Test
    void testDateCheck() throws ParseException{
        lenient().when(quizRepository.findByQuizId(2)).thenReturn(quiz);
        boolean True = aksesQuizService.dateCheck(2);
        assertEquals(true,True);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy'T'HH:mm");
        quiz.setEndDate(sdf.parse("10-10-2022T11:11"));
        boolean False = aksesQuizService.dateCheck(2);
        assertEquals(false,False);
    }


    @Test
    void testGetLimitTime(){
        lenient().when(quizRepository.findByQuizId(2)).thenReturn(quiz);
        String test = aksesQuizService.getLimitTime(2);
        assertEquals("10/10/2012 11:11:00",test);
    }


    @Test
    void testExpired()throws Exception{
        lenient().when(pesertaQuizRepository.findByPqid(1)).thenReturn(pesertaQuiz);
        boolean True = aksesQuizService.expired("10/10/2012 11:11:00",1);
        assertEquals(true,True);
        boolean False = aksesQuizService.expired("10/10/2111 11:11:00",1);
        assertEquals(false,False);
    }

    @Test
    void testFindQuiz()throws Exception{
        lenient().when(quizRepository.findByQuizId(2)).thenReturn(quiz);
        var quizHasil = aksesQuizService.findQuiz(2);
        assertEquals(quiz,quizHasil);
    }

    @Test
    void testSavePQ()throws Exception{
        lenient().when(pesertaQuizRepository.findByPqid(1)).thenReturn(pesertaQuiz);
        aksesQuizService.savePQ("A",1,1);
        verify(pesertaQuizRepository, times(1)).save( pesertaQuiz);
    }

    @Test
    void testLastQuestion()throws Exception{
        lenient().when(quizRepository.findByQuizId(1)).thenReturn(quiz);
        boolean False = aksesQuizService.lastQuestion(1,1);
        assertEquals(false,False);
        boolean True = aksesQuizService.lastQuestion(1,2);
        assertEquals(true,True);
    }

    @Test
    void testGetJawabanSoal()throws Exception{
        jawabanRepository.save(jawaban);
        List<Jawaban> listJawab = new ArrayList<>();
        listJawab.add(jawaban);
        lenient().when(jawabanRepository.findByPertanyaan(pertanyaan)).thenReturn(listJawab);
        lenient().when(pertanyaanService.getPertanyaanByNumber(2,2)).thenReturn(pertanyaan);
        List<String> listDapat = aksesQuizService.getJawabanBySoal(2,2);
        assertEquals("Pulau Sumatra",listDapat.get(0));
    }

    @Test
    void testCreatetPQ()throws Exception{
        List<PesertaQuiz> pqTmp = new ArrayList<PesertaQuiz>();
        Iterable<PesertaQuiz> keluar = pqTmp;
        lenient().when(quizRepository.findByQuizId(2)).thenReturn(quiz);
        lenient().when(pesertaQuizRepository.findByQuizIdAndIdUser(2,1)).thenReturn(pqTmp);
        int hasil = aksesQuizService.createPQ(2,1);
        assertEquals(0,hasil);
        pqTmp.add(pesertaQuiz);
        keluar = pqTmp;
        hasil = aksesQuizService.createPQ(2,1);
        assertEquals(-1,hasil);
    }

    @Test
    void testSetTruePeserta()throws Exception{
        lenient().when(pesertaQuizRepository.findByPqid(1)).thenReturn(pesertaQuiz);
        lenient().when(quizRepository.findByQuizId(1)).thenReturn(quiz);
        aksesQuizService.setTruePeserta(1,1);
        verify(pesertaQuizRepository, times(2)).save( pesertaQuiz);
    }

    @Test
    void testGetQuizFromKelas()throws Exception{
        lenient().when(kelasRepository.findByid(1)).thenReturn(kelas);
        List<Quiz>returnQuiz = aksesQuizService.getQuizFromKelas(1);
        List<Quiz> quizTmp = new ArrayList<>();
        quizTmp.add(quiz);
        kelas.setQuizzes(quizTmp);
        assertEquals(quizTmp,returnQuiz);
    }
}
