package com.projectA5.pacilclass.buat_quiz.controller;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.projectA5.pacilclass.administrasi_kelas.model.Kelas;
import com.projectA5.pacilclass.administrasi_kelas.repository.KelasRepository;
import com.projectA5.pacilclass.authentication.service.MyUserDetailsService;
import com.projectA5.pacilclass.buat_quiz.model.Quiz;
import com.projectA5.pacilclass.buat_quiz.model.dto.JawabanDto;
import com.projectA5.pacilclass.buat_quiz.model.dto.PertanyaanDto;
import com.projectA5.pacilclass.buat_quiz.model.dto.QuizDto;
import com.projectA5.pacilclass.buat_quiz.model.dto.ResponseDto;
import com.projectA5.pacilclass.buat_quiz.service.PertanyaanServiceImpl;
import com.projectA5.pacilclass.buat_quiz.service.QuizServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.parameters.P;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = QuizController.class)
public class QuizControllerTest {
    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @MockBean
    private MyUserDetailsService userDetailsService;

    @MockBean
    private QuizServiceImpl quizService;

    @MockBean
    private PertanyaanServiceImpl pertanyaanService;

    @MockBean
    private KelasRepository kelasRepository;

    private QuizDto quizDto;

    private Kelas kelas;

    private ResponseDto responseDto;

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @BeforeEach
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).apply(springSecurity()).build();

        kelas = new Kelas(1, "Agung", "Adprog A", "Advance Programming 2021", "12345");

        quizDto = new QuizDto();
        quizDto.setQuizName("QUIZ 1");
        quizDto.setTimeLimit(30);
        quizDto.setStartDate("2021-06-10T10:10");
        quizDto.setEndDate("2021-06-10T10:40");
    }


    @Test
    @WithMockUser(username = "budi")
    public void testCreateRestQuiz() throws Exception {
        mockMvc.perform(get("/buat-quiz?kelasId=1"))
                    .andExpect(status().isOk())
                    .andExpect(model().attribute("id", 1))
                    .andExpect(view().name("buat_quiz/add_quiz_new"));
    }

    @Test
    @WithMockUser(username = "budi")
    public void testCheckQuizForm() throws Exception {
        when(quizService.validate(any(QuizDto.class))).thenReturn(quizDto);

        mockMvc.perform(post("/buat-quiz/validate")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(quizDto)))
                .andExpect(jsonPath("$.quizName").value("QUIZ 1"));

    }

    @Test
    @WithMockUser(username = "budi")
    public void testSaveQuiz() throws Exception {
        PertanyaanDto pertanyaanDto = new PertanyaanDto();
        pertanyaanDto.setQuestion("1+1?");
        pertanyaanDto.setAnswerKey("A");
        pertanyaanDto.setQuestionNumber(1);

        quizDto.getQuestions().add(pertanyaanDto);

        Quiz q = quizService.createQuizDto(quizDto);

        when(quizService.createQuiz(any(Quiz.class))).thenReturn(q);
        when(kelasRepository.findByid(1)).thenReturn(kelas);

        mockMvc.perform(post("/buat-quiz/save?id=1")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(quizDto)))
                .andExpect(jsonPath("$.url").value("/administrasi_kelas/kelas?id=1"));
    }

}
