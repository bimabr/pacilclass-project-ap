package com.projectA5.pacilclass.buat_quiz.service;

import com.projectA5.pacilclass.buat_quiz.model.Jawaban;
import com.projectA5.pacilclass.buat_quiz.model.Pertanyaan;
import com.projectA5.pacilclass.buat_quiz.repository.JawabanRepository;
import com.projectA5.pacilclass.buat_quiz.repository.PertanyaanRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.stereotype.Service;

import static org.mockito.Mockito.lenient;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class JawabanServiceImplTest {

    @Mock
    private PertanyaanRepository pertanyaanRepository;

    @Mock
    private JawabanRepository jawabanRepository;

    @InjectMocks
    private JawabanServiceImpl jawabanService;

    private Jawaban jawaban;
    private Pertanyaan pertanyaan;

    @BeforeEach
    public void setUp() {
        pertanyaan = new Pertanyaan();
        pertanyaan.setPertanyaanId(1);
        pertanyaanRepository.save(pertanyaan);

        jawaban = new Jawaban();
        jawaban.setJawabanId(1);
        jawaban.setOpsiJawaban("A");
        jawaban.setTeksJawaban("Pulau Sumatra");
        jawaban.setPertanyaan(pertanyaan);
    }

    @Test
    public void testCreateJawaban() {
        when(pertanyaanRepository.findByPertanyaanId(1)).thenReturn(pertanyaan);
        lenient().when(jawabanService.createJawaban(1, jawaban)).thenReturn(jawaban);
    }

    @Test
    public void testGetJawabanById() {
        lenient().when(jawabanService.getJawaban(1)).thenReturn(jawaban);
        Jawaban res = jawabanService.getJawaban(jawaban.getJawabanId());
        assertEquals(res, jawaban);
    }

    @Test
    public void testDeleteJawabanById() {
        when(pertanyaanRepository.findByPertanyaanId(1)).thenReturn(pertanyaan);
        jawabanService.createJawaban(1, jawaban);
        jawabanService.deleteJawaban(jawaban.getJawabanId());
        lenient().when(jawabanService.getJawaban(1)).thenReturn(null);
        assertEquals(null, jawabanService.getJawaban(jawaban.getJawabanId()));
    }

    @Test
    public void testUpdateJawaban() {
        when(pertanyaanRepository.findByPertanyaanId(1)).thenReturn(pertanyaan);
        jawabanService.createJawaban(1, jawaban);
        String currentText = jawaban.getTeksJawaban();

        jawaban.setTeksJawaban("Pulau Jawa");

        lenient().when(jawabanService.updateJawaban(jawaban)).thenReturn(jawaban);
        Jawaban res = jawabanService.updateJawaban(jawaban);

        assertNotEquals(jawaban.getTeksJawaban(), currentText);
        assertEquals(jawaban.getOpsiJawaban(), jawaban.getOpsiJawaban());
    }
}