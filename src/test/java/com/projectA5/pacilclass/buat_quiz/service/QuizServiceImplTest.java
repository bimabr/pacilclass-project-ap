package com.projectA5.pacilclass.buat_quiz.service;

import com.projectA5.pacilclass.buat_quiz.model.Pertanyaan;
import com.projectA5.pacilclass.buat_quiz.model.Quiz;
import com.projectA5.pacilclass.buat_quiz.model.dto.QuizDto;
import com.projectA5.pacilclass.buat_quiz.repository.QuizRepository;
import com.projectA5.pacilclass.buat_quiz.service.error.InvalidDateException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class QuizServiceImplTest {

    @Mock
    private QuizRepository quizRepository;

    @InjectMocks
    private QuizServiceImpl quizService;

    private Quiz quiz;
    private QuizDto quizDto;

    @BeforeEach
    public void setUp() throws ParseException {
        quiz = new Quiz();
        quiz.setQuizId(1);
        quiz.setQuizName("QUIZ 1");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        quiz.setStartDate(sdf.parse("2021-06-10T10:10"));
        quiz.setEndDate(sdf.parse("2021-06-10T10:40"));
        quiz.setTimeLimit(30);

        Pertanyaan p1 = new Pertanyaan();
        Pertanyaan p2 = new Pertanyaan();
        quiz.setPertanyaans(Arrays.asList(p1, p2));

        // DTO
        quizDto = new QuizDto();
        quizDto.setQuizName("QUIZ 2");
        quizDto.setStartDate("2021-06-10T10:10");
        quizDto.setEndDate("2021-06-10T10:40");
        quizDto.setTimeLimit(30);
    }

    @Test
    public void testCreateQuiz() {
        lenient().when(quizService.createQuiz(quiz)).thenReturn(quiz);
        Quiz q = quizService.createQuiz(quiz);
        assertEquals(q.getQuizId(), quiz.getQuizId());
    }

    @Test
    public void testGetQuizById() {
        lenient().when(quizService.getQuizById(1)).thenReturn(quiz);
        Quiz res = quizService.getQuizById(quiz.getQuizId());
        assertEquals(quiz.getQuizId(), res.getQuizId());
    }

    @Test
    public void testGetQuizByName() {
        lenient().when(quizService.getQuizByName("QUIZ 1")).thenReturn(quiz);
        Quiz res = quizService.getQuizByName(quiz.getQuizName());
        assertEquals(quiz.getQuizName(), res.getQuizName());
    }

    @Test
    public void testUpdateQuiz() {
        quizService.createQuiz(quiz);
        Quiz updatedQuiz = new Quiz();
        updatedQuiz.setQuizName(quiz.getQuizName());
        updatedQuiz.setTimeLimit(60);
        updatedQuiz.setStartDate(quiz.getStartDate());
        updatedQuiz.setEndDate(quiz.getEndDate());
        updatedQuiz.setPertanyaans(quiz.getPertanyaans());

        when(quizRepository.findByQuizId(quiz.getQuizId())).thenReturn(quiz);
        Quiz result = quizService.updateQuiz(quiz.getQuizId(), updatedQuiz);
        assertEquals(quiz.getTimeLimit(), result.getTimeLimit());
    }

    @Test
    public void testGetStartDate() {
        when(quizRepository.findByQuizId(quiz.getQuizId())).thenReturn(quiz);
        assertEquals(quizService.getStartDate(quiz.getQuizId()), quiz.getStartDate());
    }

    @Test
    public void testGetEndDate() {
        when(quizRepository.findByQuizId(quiz.getQuizId())).thenReturn(quiz);
        assertEquals(quizService.getEndDate(quiz.getQuizId()), quiz.getEndDate());
    }

    @Test
    public void testGetTimeLimit() {
        when(quizRepository.findByQuizId(quiz.getQuizId())).thenReturn(quiz);
        assertEquals(quizService.getTimeLimit(quiz.getQuizId()), quiz.getTimeLimit());
    }

    @Test
    public void testDeleteQuiz() {
        quizService.createQuiz(quiz);
        quizService.deleteQuiz(quiz.getQuizId());
        assertNull(quizService.getQuizById(quiz.getQuizId()));
    }

    // DTO SECTION
    @Test
    public void testCreateQuizFromDTO() throws ParseException {

        Quiz newQuiz = new Quiz();
        newQuiz.setQuizName(quizDto.getQuizName());
        newQuiz.setTimeLimit(quizDto.getTimeLimit());

        when(quizRepository.save(Mockito.any(Quiz.class))).thenReturn(newQuiz);
        assertEquals(quizService.createQuizDto(quizDto).getQuizName(), quizDto.getQuizName());
    }

    @Test
    public void testValidQuizDTO() throws ParseException {
        assertEquals(quizDto, quizService.validate(quizDto));
    }

    @Test
    public void testInvalidQuizDTO() throws ParseException {
        String startDate = quizDto.getStartDate();
        quizDto.setStartDate(quizDto.getEndDate());
        quizDto.setEndDate(startDate);

        assertThrows(InvalidDateException.class, () -> {
            quizService.validate(quizDto);
        });
    }

}