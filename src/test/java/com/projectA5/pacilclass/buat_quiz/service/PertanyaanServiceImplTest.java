package com.projectA5.pacilclass.buat_quiz.service;

import com.projectA5.pacilclass.buat_quiz.model.Jawaban;
import com.projectA5.pacilclass.buat_quiz.model.Pertanyaan;
import com.projectA5.pacilclass.buat_quiz.model.Quiz;
import com.projectA5.pacilclass.buat_quiz.model.dto.JawabanDto;
import com.projectA5.pacilclass.buat_quiz.model.dto.PertanyaanDto;
import com.projectA5.pacilclass.buat_quiz.repository.JawabanRepository;
import com.projectA5.pacilclass.buat_quiz.repository.PertanyaanRepository;
import com.projectA5.pacilclass.buat_quiz.repository.QuizRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PertanyaanServiceImplTest {

    @Mock
    private PertanyaanRepository pertanyaanRepository;

    @Mock
    private QuizRepository quizRepository;

    @Mock
    private JawabanRepository jawabanRepository;

    @InjectMocks
    private PertanyaanServiceImpl pertanyaanService;

    private Pertanyaan pertanyaan;
    private Quiz q;

    @BeforeEach
    public void setUp() {
        q = new Quiz();
        q.setQuizId(1);
        quizRepository.save(q);

        pertanyaan = new Pertanyaan();
        pertanyaan.setPertanyaanId(1);
        pertanyaan.setAnswerKey("B");
        pertanyaan.setQuestionNumber(1);
        pertanyaan.setQuestion("Di pulau manakah Kota Medan berada?");
        q.getPertanyaans().add(pertanyaan);
    }

    @Test
    public void testCreatePertanyaan() {
        when(quizRepository.findByQuizId(q.getQuizId())).thenReturn(q);
        lenient().when(pertanyaanService.createPertanyaan(q.getQuizId(), pertanyaan)).thenReturn(pertanyaan);
    }

    @Test
    public void testGetPertanyaanByNumberAndQuizId() {
        when(quizRepository.findByQuizId(q.getQuizId())).thenReturn(q);
        pertanyaanService.createPertanyaan(q.getQuizId(), pertanyaan);
        lenient().when(pertanyaanService.getPertanyaanByNumber(1,1)).thenReturn(pertanyaan);
        Pertanyaan res = pertanyaanService.getPertanyaanByNumber(
                pertanyaan.getQuestionNumber(),pertanyaan.getQuiz().getQuizId());
        assertEquals(res, res);
    }

    @Test
    public void testDeletePertanyaan() {
        when(quizRepository.findByQuizId(q.getQuizId())).thenReturn(q);
        pertanyaanService.createPertanyaan(q.getQuizId(), pertanyaan);
        pertanyaanService.deletePertanyaan(pertanyaan.getPertanyaanId());
        lenient().when(pertanyaanService.getPertanyaanByNumber(1,1)).thenReturn(null);
        assertEquals(null, pertanyaanService.getPertanyaanByNumber(1,1));
    }

    @Test
    public void testGetListJawaban() {
        Jawaban jawaban = new Jawaban();
        jawaban.setJawabanId(1);
        jawaban.setPertanyaan(pertanyaan);
        jawaban.setTeksJawaban("Papua");
        jawaban.setOpsiJawaban("A");

        pertanyaan.getJawabans().add(jawaban);
        when(pertanyaanRepository.findByPertanyaanId(pertanyaan.getPertanyaanId())).thenReturn(pertanyaan);
        assertEquals(pertanyaanService.getListJawaban(pertanyaan.getPertanyaanId()), pertanyaan.getJawabans());
    }

    // DTO

    @Test
    public void testCreatePertanyaanFromDTO() {
        List<PertanyaanDto> dtoList = new ArrayList<>();

        JawabanDto jawabanDto = new JawabanDto();
        jawabanDto.setOpsiJawaban("B");
        jawabanDto.setTeksJawaban("Budi");

        PertanyaanDto pertanyaanDto = new PertanyaanDto();
        pertanyaanDto.setQuestionNumber(1);
        pertanyaanDto.setQuestion("Siapa namaku?");
        pertanyaanDto.setAnswerKey("B");
        pertanyaanDto.getAnswers().add(jawabanDto);

        dtoList.add(pertanyaanDto);

        pertanyaanService.createPertanyaanFromDto(dtoList, q);
        verify(pertanyaanRepository, times(1)).save(Mockito.any(Pertanyaan.class));
    }

}