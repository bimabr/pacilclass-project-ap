package com.projectA5.pacilclass.announcement.controller;

import com.projectA5.pacilclass.announcement.repository.AnnouncementRepository;
import com.projectA5.pacilclass.announcement.model.Announcement;
import com.projectA5.pacilclass.authentication.repository.UserRepository;
import com.projectA5.pacilclass.administrasi_kelas.service.AdministrasiKelasService;
import com.projectA5.pacilclass.announcement.service.AnnouncementImplService;
import com.projectA5.pacilclass.announcement.repository.NotificationRepository;
import com.projectA5.pacilclass.authentication.model.BaseUser;
import com.projectA5.pacilclass.administrasi_kelas.model.Kelas;

import com.projectA5.pacilclass.authentication.service.MyUserDetailsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;

import static org.mockito.Mockito.lenient;

import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;

@WebMvcTest(controllers = AnnouncementController.class)
public class AnnouncementControllerTest {
    @Autowired
    private WebApplicationContext webApplicationContext;


    @MockBean
    private MyUserDetailsService userDetailsService;

    @MockBean
    private AnnouncementRepository announcementRepository;

    @MockBean
    private AnnouncementImplService announcementImplService;

    @MockBean
    private AdministrasiKelasService administrasiKelasService;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private Announcement announcement;

    @MockBean
    private NotificationRepository notificationRepository;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).apply(springSecurity()).build();
    }

    @Test
    @WithMockUser(roles = {"USER"})
    public void testForm() throws Exception {
        this.mockMvc.perform(get("/announcement/form"))
                .andExpect(status().isOk())
                .andExpect(view().name("announcement/announcement_form"));
    }

    @Test
    @WithMockUser(roles = {"USER"})
    public void testRedirect() throws Exception {
        this.mockMvc.perform(get("/announcement"))
                .andExpect(redirectedUrl("/announcement/form"));
    }

    @Test
    @WithMockUser(roles = {"USER"})
    void testCreateAnnouncement() throws Exception {
        Announcement announcement = new Announcement();
        announcement.setKelasId(1);

        List<BaseUser> pesertas = new ArrayList();
        pesertas.add(new BaseUser());
        Kelas kelas = new Kelas();
        kelas.setPesertas(pesertas);

        lenient().when(administrasiKelasService.getKelasbyid(1)).thenReturn(kelas);
        mockMvc.perform(post("/announcement/create_announcement")
                .flashAttr("announcement", announcement));
    }

    @Test
    @WithMockUser(roles = {"USER"})
    public void testGetNotification() throws Exception {
        HashMap<String, Object> session = new HashMap();
        session.put("userId", 1);

        BaseUser user = new BaseUser();
        user.setUserId(1);
        user.setUsername("Bambang");
        user.setPassword("password");

        lenient().when(userRepository.findByUserId(1)).thenReturn(user);

        this.mockMvc.perform(
                    get("/announcement/notification")
                    .sessionAttrs(session)
                )
                .andExpect(status().isOk())
                .andExpect(view().name("announcement/notification"));
    }

}
