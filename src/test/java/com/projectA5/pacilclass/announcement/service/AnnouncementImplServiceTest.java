package com.projectA5.pacilclass.announcement.service;

import com.projectA5.pacilclass.announcement.model.Announcement;
import com.projectA5.pacilclass.announcement.repository.AnnouncementRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;

import org.springframework.data.domain.PageImpl;
import org.mockito.ArgumentCaptor;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.lenient;

import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)
class AnnouncementImplServiceTest {
    @Mock
    private AnnouncementRepository announcementRepository;

    @InjectMocks
    private AnnouncementImplService announcementImplService;

    private Announcement announcement;

    @BeforeEach
    public void setUp() {
        announcement = new Announcement();
    }

    @Test
    void testServiceCreateAnnouncement(){
        Announcement announcement = new Announcement();
        lenient().when(announcementImplService.createAnnouncement(announcement)).thenReturn(announcement);
    }

    @Test
    void testServiceGetAllAnnouncements(){
        Announcement announcement = new Announcement();
        announcementImplService.createAnnouncement(announcement);

        ArrayList<Announcement> list = new ArrayList();
        list.add(announcement);

        lenient().when(announcementImplService.getAllAnnouncements()).thenReturn(list);
    }

    @Test
    void testServiceGetAnnouncementsByKelasId(){
        Announcement announcement = new Announcement();
        announcement.setKelasId(1);
        announcementImplService.createAnnouncement(announcement);

        ArrayList<Announcement> list = new ArrayList();
        list.add(announcement);

        lenient().when(announcementImplService.getAnnouncementsByKelasId(1)).thenReturn(list);
    }
}