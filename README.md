## GROUP PROJECT ADVANCE PROGRAMMING - A5
### App Name : PACIL CLASS


[![pipeline status](https://gitlab.com/bimabr/pacilclass-project-ap/badges/master/pipeline.svg)](https://gitlab.com/bimabr/pacilclass-project-ap/-/commits/master)


[![coverage report](https://gitlab.com/bimabr/pacilclass-project-ap/badges/master/coverage.svg)](https://gitlab.com/bimabr/pacilclass-project-ap/-/commits/master)

- Bima Bagas R
- Michael Felix
- Robby NF
- Nadhira R
- Donny Samuel

### [NOTE] : Please check "application.properties" before running the app
